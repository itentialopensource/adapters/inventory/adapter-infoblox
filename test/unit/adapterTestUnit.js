/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-infoblox',
      type: 'Infoblox',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Infoblox = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Infoblox Adapter Test', () => {
  describe('Infoblox Class Tests', () => {
    const a = new Infoblox(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('infoblox'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('infoblox'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Infoblox', pronghornDotJson.export);
          assert.equal('Infoblox', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-infoblox', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('infoblox'));
          assert.equal('Infoblox', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-infoblox', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-infoblox', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createNetwork - errors', () => {
      it('should have a createNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no network', (done) => {
        try {
          a.createNetwork(null, null, (data, error) => {
            try {
              const displayE = 'network is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no comment', (done) => {
        try {
          a.createNetwork('1.1.1.1', null, (data, error) => {
            try {
              const displayE = 'comment is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkKeyByIP - errors', () => {
      it('should have a getNetworkKeyByIP function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkKeyByIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthZone - errors', () => {
      it('should have a createAuthZone function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no fqdnName', (done) => {
        try {
          a.createAuthZone(null, (data, error) => {
            try {
              const displayE = 'fqdnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createAuthZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHostRecord - errors', () => {
      it('should have a createHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.createHostRecord(null, null, null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no IPAddress', (done) => {
        try {
          a.createHostRecord('itential.com', null, null, (data, error) => {
            try {
              const displayE = 'IPAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHostRecord2 - errors', () => {
      it('should have a createHostRecord2 function', (done) => {
        try {
          assert.equal(true, typeof a.createHostRecord2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.createHostRecord2(null, null, null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createHostRecord2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no IPAddress', (done) => {
        try {
          a.createHostRecord2('itential.com', null, null, (data, error) => {
            try {
              const displayE = 'IPAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createHostRecord2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpByHost - errors', () => {
      it('should have a getIpByHost function', (done) => {
        try {
          assert.equal(true, typeof a.getIpByHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.getIpByHost(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getIpByHostWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostKeyByHostName - errors', () => {
      it('should have a getHostKeyByHostName function', (done) => {
        try {
          assert.equal(true, typeof a.getHostKeyByHostName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.getHostKeyByHostName(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getHostKeyByHostNameWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostRecordByHostName - errors', () => {
      it('should have a deleteHostRecordByHostName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHostRecordByHostName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.deleteHostRecordByHostName(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteHostRecordByHostName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextNetwork - errors', () => {
      it('should have a assignNextNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.assignNextNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no networkBlock', (done) => {
        try {
          a.assignNextNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no nextNetworkSubnetMask', (done) => {
        try {
          a.assignNextNetwork('fakedata', null, null, (data, error) => {
            try {
              const displayE = 'nextNetworkSubnetMask is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextNetworkByNetwork - errors', () => {
      it('should have a assignNextNetworkByNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.assignNextNetworkByNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no networkBlock', (done) => {
        try {
          a.assignNextNetworkByNetwork(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextNetworkByNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no nextNetworkSubnetMask', (done) => {
        try {
          a.assignNextNetworkByNetwork('fakedata', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nextNetworkSubnetMask is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextNetworkByNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextNetworkByRef - errors', () => {
      it('should have a assignNextNetworkByRef function', (done) => {
        try {
          assert.equal(true, typeof a.assignNextNetworkByRef === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no networkBlock', (done) => {
        try {
          a.assignNextNetworkByRef(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextNetworkByRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no nextNetworkSubnetMask', (done) => {
        try {
          a.assignNextNetworkByRef('fakedata', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nextNetworkSubnetMask is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextNetworkByRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextIP - errors', () => {
      it('should have a assignNextIP function', (done) => {
        try {
          assert.equal(true, typeof a.assignNextIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no networkIP', (done) => {
        try {
          a.assignNextIP(null, ' ', ' ', (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.assignNextIP(' ', null, ' ', (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no fqdnName', (done) => {
        try {
          a.assignNextIP(' ', ' ', null, (data, error) => {
            try {
              const displayE = 'fqdnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-assignNextIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthZoneDetailsByfqdnName - errors', () => {
      it('should have a getAuthZoneDetailsByfqdnName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthZoneDetailsByfqdnName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.getAuthZoneDetailsByfqdnName(null, (data, error) => {
            try {
              const displayE = 'fqdnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getAuthZoneDetailsByfqdnNameWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getForwardZones - errors', () => {
      it('should have a getForwardZones function', (done) => {
        try {
          assert.equal(true, typeof a.getForwardZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createForwardZone - errors', () => {
      it('should have a createForwardZone function', (done) => {
        try {
          assert.equal(true, typeof a.createForwardZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createForwardZone(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createForwardZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZones - errors', () => {
      it('should have a deleteZones function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.deleteZones(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneDelegations - errors', () => {
      it('should have a getZoneDelegations function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneDelegations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneDelegation - errors', () => {
      it('should have a createZoneDelegation function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneDelegation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneDelegation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneDelegation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthZoneByRef - errors', () => {
      it('should have a deleteAuthZoneByRef function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthZoneByRef === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneRef', (done) => {
        try {
          a.deleteAuthZoneByRef(null, (data, error) => {
            try {
              const displayE = 'zoneRef is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteAuthZoneByRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBlock - errors', () => {
      it('should have a getNetworkBlock function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.getNetworkBlock(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkBlockWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyNetworkBlock - errors', () => {
      it('should have a modifyNetworkBlock function', (done) => {
        try {
          assert.equal(true, typeof a.modifyNetworkBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkIP', (done) => {
        try {
          a.modifyNetworkBlock(null, null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-modifyNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing comment', (done) => {
        try {
          a.modifyNetworkBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'comment is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-modifyNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContainerNextNetworkIps - errors', () => {
      it('should have a getNetworkContainerNextNetworkIps function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkContainerNextNetworkIps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkContainerNextNetworkIps(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerId', (done) => {
        try {
          a.getNetworkContainerNextNetworkIps('fakedata', null, null, null, (data, error) => {
            try {
              const displayE = 'containerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getNetworkContainerNextNetworkIps('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getNetworkContainerNextNetworkIps('fakedata', 'fakedata', { key: 'fakedata' }, null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpv6NetworkContainerNextNetworkIps - errors', () => {
      it('should have a getIpv6NetworkContainerNextNetworkIps function', (done) => {
        try {
          assert.equal(true, typeof a.getIpv6NetworkContainerNextNetworkIps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getIpv6NetworkContainerNextNetworkIps(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getIpv6NetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerId', (done) => {
        try {
          a.getIpv6NetworkContainerNextNetworkIps('fakedata', null, null, null, (data, error) => {
            try {
              const displayE = 'containerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getIpv6NetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getIpv6NetworkContainerNextNetworkIps('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getIpv6NetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing query', (done) => {
        try {
          a.getIpv6NetworkContainerNextNetworkIps('fakedata', 'fakedata', { key: 'fakedata' }, null, (data, error) => {
            try {
              const displayE = 'query is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getIpv6NetworkContainerNextNetworkIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDetails - errors', () => {
      it('should have a getNetworkDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContainerDetails - errors', () => {
      it('should have a getNetworkContainerDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkContainerDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.getNetworkContainerDetails(null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkContainerDetailsWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkContainers - errors', () => {
      it('should have a listNetworkContainers function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkContainers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkContainer - errors', () => {
      it('should have a createNetworkContainer function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkContainer - errors', () => {
      it('should have a deleteNetworkContainer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.deleteNetworkContainer(null, (data, error) => {
            try {
              const displayE = 'networkRef is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNetworkContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#genericCreateNextAvailableNetwork - errors', () => {
      it('should have a genericCreateNextAvailableNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.genericCreateNextAvailableNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.genericCreateNextAvailableNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkRef is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-genericCreateNextAvailableNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing options', (done) => {
        try {
          a.genericCreateNextAvailableNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'options is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-genericCreateNextAvailableNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.genericCreateNextAvailableNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-genericCreateNextAvailableNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should have a deleteNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.deleteNetwork(null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkContainerDetailsWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkv2 - errors', () => {
      it('should have a deleteNetworkv2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkv2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.deleteNetworkv2(null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNetworkv2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkUtilizationByIP - errors', () => {
      it('should have a getNetworkUtilizationByIP function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkUtilizationByIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.getNetworkUtilizationByIP(null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkUtilizationByIPWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkBlock - errors', () => {
      it('should have a createNetworkBlock function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing function', (done) => {
        try {
          a.createNetworkBlock(null, null, null, (data, error) => {
            try {
              const displayE = 'functionParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.createNetworkBlock('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkBlock('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtcServer - errors', () => {
      it('should have a getDtcServer function', (done) => {
        try {
          assert.equal(true, typeof a.getDtcServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDtcServer - errors', () => {
      it('should have a createDtcServer function', (done) => {
        try {
          assert.equal(true, typeof a.createDtcServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDtcServer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDtcServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtcPool - errors', () => {
      it('should have a getDtcPool function', (done) => {
        try {
          assert.equal(true, typeof a.getDtcPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDtcPool - errors', () => {
      it('should have a createDtcPool function', (done) => {
        try {
          assert.equal(true, typeof a.createDtcPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDtcPool(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDtcPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDtcLbdn - errors', () => {
      it('should have a createDtcLbdn function', (done) => {
        try {
          assert.equal(true, typeof a.createDtcLbdn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDtcLbdn(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDtcLbdn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostRecord - errors', () => {
      it('should have a getHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.getHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostName', (done) => {
        try {
          a.getHostRecord(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getHostRecordWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getARecords - errors', () => {
      it('should have a getARecords function', (done) => {
        try {
          assert.equal(true, typeof a.getARecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createARecord - errors', () => {
      it('should have a createARecord function', (done) => {
        try {
          assert.equal(true, typeof a.createARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createARecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCnameRecords - errors', () => {
      it('should have a getCnameRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getCnameRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCNAMERecord - errors', () => {
      it('should have a createCNAMERecord function', (done) => {
        try {
          assert.equal(true, typeof a.createCNAMERecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCNAMERecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTxtRecords - errors', () => {
      it('should have a getTxtRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getTxtRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTxtRecord - errors', () => {
      it('should have a createTxtRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createTxtRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTxtRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createTxtRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRecords - errors', () => {
      it('should have a getAllRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.getAllRecords(null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getAllRecordsWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaaRecords - errors', () => {
      it('should have a getAaaaRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAaaaRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAaaaRecord - errors', () => {
      it('should have a createAaaaRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createAaaaRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAaaaRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createAaaaRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMxRecords - errors', () => {
      it('should have a getMxRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getMxRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMxRecord - errors', () => {
      it('should have a createMxRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createMxRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMxRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createMxRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsRecords - errors', () => {
      it('should have a getNsRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getNsRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNsRecord - errors', () => {
      it('should have a createNsRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createNsRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNsRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNsRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPtrRecords - errors', () => {
      it('should have a getPtrRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getPtrRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPtrRecord - errors', () => {
      it('should have a createPtrRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createPtrRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPtrRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createPtrRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrvRecords - errors', () => {
      it('should have a getSrvRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getSrvRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSrvRecord - errors', () => {
      it('should have a createSrvRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSrvRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSrvRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSrvRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHostRecord - errors', () => {
      it('should have a updateHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.updateHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updateHostRecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHostRecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostRecord - errors', () => {
      it('should have a deleteHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deleteHostRecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateARecord - errors', () => {
      it('should have a updateARecord function', (done) => {
        try {
          assert.equal(true, typeof a.updateARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updateARecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateARecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteARecord - errors', () => {
      it('should have a deleteARecord function', (done) => {
        try {
          assert.equal(true, typeof a.deleteARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deleteARecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePTRRecord - errors', () => {
      it('should have a updatePTRRecord function', (done) => {
        try {
          assert.equal(true, typeof a.updatePTRRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updatePTRRecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updatePTRRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePTRRecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updatePTRRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePTRRecord - errors', () => {
      it('should have a deletePTRRecord function', (done) => {
        try {
          assert.equal(true, typeof a.deletePTRRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deletePTRRecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deletePTRRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCNAMERecord - errors', () => {
      it('should have a updateCNAMERecord function', (done) => {
        try {
          assert.equal(true, typeof a.updateCNAMERecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updateCNAMERecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCNAMERecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCNAMERecord - errors', () => {
      it('should have a deleteCNAMERecord function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCNAMERecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deleteCNAMERecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridDns - errors', () => {
      it('should have a getGridDns function', (done) => {
        try {
          assert.equal(true, typeof a.getGridDns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDns - errors', () => {
      it('should have a getMemberDns function', (done) => {
        try {
          assert.equal(true, typeof a.getMemberDns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResponsePolicyZones - errors', () => {
      it('should have a getResponsePolicyZones function', (done) => {
        try {
          assert.equal(true, typeof a.getResponsePolicyZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResponsePolicyZone - errors', () => {
      it('should have a createResponsePolicyZone function', (done) => {
        try {
          assert.equal(true, typeof a.createResponsePolicyZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createResponsePolicyZone(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createResponsePolicyZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSubstituitionRuleForARecords - errors', () => {
      it('should have a createSubstituitionRuleForARecords function', (done) => {
        try {
          assert.equal(true, typeof a.createSubstituitionRuleForARecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSubstituitionRuleForARecords(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSubstituitionRuleForARecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituitionRuleForPtrRecords - errors', () => {
      it('should have a addSubstituitionRuleForPtrRecords function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituitionRuleForPtrRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituitionRuleForPtrRecords(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addSubstituitionRuleForPtrRecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituitionRuleForIpTriggerPolicy - errors', () => {
      it('should have a addSubstituitionRuleForIpTriggerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituitionRuleForIpTriggerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituitionRuleForIpTriggerPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addSubstituitionRuleForIpTriggerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBlockDomainNameRule - errors', () => {
      it('should have a addBlockDomainNameRule function', (done) => {
        try {
          assert.equal(true, typeof a.addBlockDomainNameRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBlockDomainNameRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addBlockDomainNameRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBlockClientIpAddressRule - errors', () => {
      it('should have a addBlockClientIpAddressRule function', (done) => {
        try {
          assert.equal(true, typeof a.addBlockClientIpAddressRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBlockClientIpAddressRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addBlockClientIpAddressRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituteDomainNameClientIpAddressRule - errors', () => {
      it('should have a addSubstituteDomainNameClientIpAddressRule function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituteDomainNameClientIpAddressRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituteDomainNameClientIpAddressRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addSubstituteDomainNameClientIpAddressRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBlockIpAddressNoSuchDomainRule - errors', () => {
      it('should have a addBlockIpAddressNoSuchDomainRule function', (done) => {
        try {
          assert.equal(true, typeof a.addBlockIpAddressNoSuchDomainRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBlockIpAddressNoSuchDomainRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addBlockIpAddressNoSuchDomainRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituteDomainNameIpAddressRule - errors', () => {
      it('should have a addSubstituteDomainNameIpAddressRule function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituteDomainNameIpAddressRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituteDomainNameIpAddressRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addSubstituteDomainNameIpAddressRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRpzRecords - errors', () => {
      it('should have a getAllRpzRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRpzRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.getAllRpzRecords(null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getAllRpzRecordsWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNameServerGroups - errors', () => {
      it('should have a getNameServerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getNameServerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNameServerGroup - errors', () => {
      it('should have a createNameServerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createNameServerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNameServerGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNameServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRangeByExtensibleAttribute - errors', () => {
      it('should have a getRangeByExtensibleAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.getRangeByExtensibleAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.getRangeByExtensibleAttribute(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getRangeByExtensibleAttributeWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRange - errors', () => {
      it('should have a createRange function', (done) => {
        try {
          assert.equal(true, typeof a.createRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRange(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLeaseByIpAddress - errors', () => {
      it('should have a getLeaseByIpAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getLeaseByIpAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.getLeaseByIpAddress(null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getLeaseByIpAddressWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDhcp - errors', () => {
      it('should have a getMemberDhcp function', (done) => {
        try {
          assert.equal(true, typeof a.getMemberDhcp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpDetails - errors', () => {
      it('should have a getIpDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getIpDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpAddressUsingSearch - errors', () => {
      it('should have a getIpAddressUsingSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getIpAddressUsingSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.getIpAddressUsingSearch(null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getIpAddressUsingSearchWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsView - errors', () => {
      it('should have a getDnsView function', (done) => {
        try {
          assert.equal(true, typeof a.getDnsView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkView - errors', () => {
      it('should have a getNetworkView function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkViewWithQuery - errors', () => {
      it('should have a getNetworkViewWithQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkViewWithQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkView - errors', () => {
      it('should have a createNetworkView function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkView(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNetworkView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkViewById - errors', () => {
      it('should have a getNetworkViewById function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkViewById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing viewId', (done) => {
        try {
          a.getNetworkViewById(null, null, (data, error) => {
            try {
              const displayE = 'viewId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkViewById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkView - errors', () => {
      it('should have a updateNetworkView function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing viewId', (done) => {
        try {
          a.updateNetworkView(null, (data, error) => {
            try {
              const displayE = 'viewId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNetworkView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkView - errors', () => {
      it('should have a deleteNetworkView function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing viewId', (done) => {
        try {
          a.deleteNetworkView(null, (data, error) => {
            try {
              const displayE = 'viewId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNetworkView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFixedAddressMac - errors', () => {
      it('should have a getFixedAddressMac function', (done) => {
        try {
          assert.equal(true, typeof a.getFixedAddressMac === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mac', (done) => {
        try {
          a.getFixedAddressMac(null, (data, error) => {
            try {
              const displayE = 'mac is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getFixedAddressMacWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFixedAddress - errors', () => {
      it('should have a createFixedAddress function', (done) => {
        try {
          assert.equal(true, typeof a.createFixedAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFixedAddress(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createFixedAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembers - errors', () => {
      it('should have a getMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMember - errors', () => {
      it('should have a createMember function', (done) => {
        try {
          assert.equal(true, typeof a.createMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMember(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGrid - errors', () => {
      it('should have a getGrid function', (done) => {
        try {
          assert.equal(true, typeof a.getGrid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridStatus - errors', () => {
      it('should have a getGridStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getGridStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridPendingChanges - errors', () => {
      it('should have a getGridPendingChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getGridPendingChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVdiscoveryTasks - errors', () => {
      it('should have a getVdiscoveryTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getVdiscoveryTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVdiscoveryTask - errors', () => {
      it('should have a createVdiscoveryTask function', (done) => {
        try {
          assert.equal(true, typeof a.createVdiscoveryTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVdiscoveryTask(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createVdiscoveryTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForARole - errors', () => {
      it('should have a getPermissionsForARole function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForARole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getPermissionsForARole(null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getPermissionsForARoleWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiSchema - errors', () => {
      it('should have a getWapiSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schema', (done) => {
        try {
          a.getWapiSchema(null, (data, error) => {
            try {
              const displayE = 'schema is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getWapiSchemaWithQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multipleRecordTypes - errors', () => {
      it('should have a multipleRecordTypes function', (done) => {
        try {
          assert.equal(true, typeof a.multipleRecordTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.multipleRecordTypes(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-multipleRecordTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinition - errors', () => {
      it('should have a getExtensibleAttributeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getExtensibleAttributeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinitionWithQuery - errors', () => {
      it('should have a getExtensibleAttributeDefinitionWithQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getExtensibleAttributeDefinitionWithQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensibleAttributeDefinition - errors', () => {
      it('should have a createExtensibleAttributeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensibleAttributeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensibleAttributeDefinition(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createExtensibleAttributeDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinitionById - errors', () => {
      it('should have a getExtensibleAttributeDefinitionById function', (done) => {
        try {
          assert.equal(true, typeof a.getExtensibleAttributeDefinitionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eaId', (done) => {
        try {
          a.getExtensibleAttributeDefinitionById(null, {}, (data, error) => {
            try {
              const displayE = 'eaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getExtensibleAttributeDefinitionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExtensibleAttributeDefinition - errors', () => {
      it('should have a updateExtensibleAttributeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.updateExtensibleAttributeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eaId', (done) => {
        try {
          a.updateExtensibleAttributeDefinition(null, (data, error) => {
            try {
              const displayE = 'eaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateExtensibleAttributeDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadFile - errors', () => {
      it('should have a uploadFile function', (done) => {
        try {
          assert.equal(true, typeof a.uploadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlFromUploadInitiation', (done) => {
        try {
          a.uploadFile(null, 'filedata', (data, error) => {
            try {
              const displayE = 'urlFromUploadInitiation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-uploadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filedata', (done) => {
        try {
          a.uploadFile('https://1.1.1.1/http_direct_file_io/req_id-UPLOAD-1111111111111111/import_records', null, (data, error) => {
            try {
              const displayE = 'filedata is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-uploadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadFile - errors', () => {
      it('should have a downloadFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing urlFromDownloadInitiation', (done) => {
        try {
          a.downloadFile(null, (data, error) => {
            try {
              const displayE = 'urlFromDownloadInitiation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-downloadFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensibleAttributeDefinition - errors', () => {
      it('should have a deleteExtensibleAttributeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensibleAttributeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eaId', (done) => {
        try {
          a.deleteExtensibleAttributeDefinition(null, (data, error) => {
            try {
              const displayE = 'eaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteExtensibleAttributeDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createObject - errors', () => {
      it('should have a createObject function', (done) => {
        try {
          assert.equal(true, typeof a.createObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.createObject(null, {}, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createObject('type', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObject - errors', () => {
      it('should have a updateObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.updateObject(null, {}, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.updateObject('noslash', {}, (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObject('type', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObject - errors', () => {
      it('should have a deleteObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.deleteObject(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.deleteObject('noslash', (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObject - errors', () => {
      it('should have a getObject function', (done) => {
        try {
          assert.equal(true, typeof a.getObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.getObject(null, {}, 'field', (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryObject', (done) => {
        try {
          a.getObject('type', null, 'field', (data, error) => {
            try {
              const displayE = 'queryObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing returnFields', (done) => {
        try {
          a.getObject('type', {}, null, (data, error) => {
            try {
              const displayE = 'returnFields is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#extractObjectTypeFromObjectReference - errors', () => {
      it('should have a extractObjectTypeFromObjectReference function', (done) => {
        try {
          assert.equal(true, typeof a.extractObjectTypeFromObjectReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.extractObjectTypeFromObjectReference(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-extractObjectTypeFromObjectReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.extractObjectTypeFromObjectReference('noslash', (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      const objectReference = 'record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx';
      it('should return correct object type', (done) => {
        try {
          a.extractObjectTypeFromObjectReference(objectReference, (data, error) => {
            try {
              assert.equal(data, 'record:host');
              assert.equal(error, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restartServices - errors', () => {
      it('should have a restartServices function', (done) => {
        try {
          assert.equal(true, typeof a.restartServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.restartServices(null, null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-restartServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      const objectReference = 'grid/b25lLmNsdXN0ZXIkMA:Infoblox';
      it('should error if - missing queries', (done) => {
        try {
          a.restartServices(objectReference, null, (data, error) => {
            try {
              const displayE = 'queries is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-restartServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      const queries = {
        _function: 'restartservices',
        restart_option: 'RESTART_IF_NEEDED'
      };
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.restartServices('noslash', queries, (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRecordsByReference - errors', () => {
      it('should have a getAllRecordsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRecordsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getAllRecordsByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getAllRecordsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhosts - errors', () => {
      it('should have a getBulkhosts function', (done) => {
        try {
          assert.equal(true, typeof a.getBulkhosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkhosts - errors', () => {
      it('should have a createBulkhosts function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkhosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkhosts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createBulkhosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhostByReference - errors', () => {
      it('should have a getBulkhostByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getBulkhostByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getBulkhostByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getBulkhostByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBulkhostByReference - errors', () => {
      it('should have a updateBulkhostByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateBulkhostByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateBulkhostByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateBulkhostByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBulkhostByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateBulkhostByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBulkhostByReference - errors', () => {
      it('should have a deleteBulkhostByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBulkhostByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteBulkhostByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteBulkhostByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhostTemplate - errors', () => {
      it('should have a getBulkhostTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getBulkhostTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkhostTemplate - errors', () => {
      it('should have a createBulkhostTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkhostTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkhostTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createBulkhostTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhostTemplateByReference - errors', () => {
      it('should have a getBulkhostTemplateByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getBulkhostTemplateByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getBulkhostTemplateByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getBulkhostTemplateByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBulkhostTemplateByReference - errors', () => {
      it('should have a updateBulkhostTemplateByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateBulkhostTemplateByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateBulkhostTemplateByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateBulkhostTemplateByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBulkhostTemplateByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateBulkhostTemplateByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBulkhostTemplateByReference - errors', () => {
      it('should have a deleteBulkhostTemplateByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBulkhostTemplateByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteBulkhostTemplateByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteBulkhostTemplateByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalCluster - errors', () => {
      it('should have a getDdnsPrincipalCluster function', (done) => {
        try {
          assert.equal(true, typeof a.getDdnsPrincipalCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDdnsPrincipalCluster - errors', () => {
      it('should have a createDdnsPrincipalCluster function', (done) => {
        try {
          assert.equal(true, typeof a.createDdnsPrincipalCluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDdnsPrincipalCluster('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDdnsPrincipalCluster', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalClusterByReference - errors', () => {
      it('should have a getDdnsPrincipalClusterByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDdnsPrincipalClusterByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDdnsPrincipalClusterByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDdnsPrincipalClusterByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDdnsPrincipalClusterByReference - errors', () => {
      it('should have a updateDdnsPrincipalClusterByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateDdnsPrincipalClusterByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateDdnsPrincipalClusterByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDdnsPrincipalClusterByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDdnsPrincipalClusterByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDdnsPrincipalClusterByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDdnsPrincipalClusterByReference - errors', () => {
      it('should have a deleteDdnsPrincipalClusterByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDdnsPrincipalClusterByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteDdnsPrincipalClusterByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteDdnsPrincipalClusterByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalClusterGroup - errors', () => {
      it('should have a getDdnsPrincipalClusterGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getDdnsPrincipalClusterGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDdnsPrincipalClusterGroup - errors', () => {
      it('should have a createDdnsPrincipalClusterGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createDdnsPrincipalClusterGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDdnsPrincipalClusterGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDdnsPrincipalClusterGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalClusterGroupByReference - errors', () => {
      it('should have a getDdnsPrincipalClusterGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDdnsPrincipalClusterGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDdnsPrincipalClusterGroupByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDdnsPrincipalClusterGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDdnsPrincipalClusterGroupByReference - errors', () => {
      it('should have a updateDdnsPrincipalClusterGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateDdnsPrincipalClusterGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateDdnsPrincipalClusterGroupByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDdnsPrincipalClusterGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDdnsPrincipalClusterGroupByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDdnsPrincipalClusterGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDdnsPrincipalClusterGroupByReference - errors', () => {
      it('should have a deleteDdnsPrincipalClusterGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDdnsPrincipalClusterGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteDdnsPrincipalClusterGroupByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteDdnsPrincipalClusterGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDns64Group - errors', () => {
      it('should have a getDns64Group function', (done) => {
        try {
          assert.equal(true, typeof a.getDns64Group === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDns64Group - errors', () => {
      it('should have a createDns64Group function', (done) => {
        try {
          assert.equal(true, typeof a.createDns64Group === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDns64Group('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDns64Group', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDns64GroupByReference - errors', () => {
      it('should have a getDns64GroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDns64GroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDns64GroupByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDns64GroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDns64GroupByReference - errors', () => {
      it('should have a updateDns64GroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateDns64GroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateDns64GroupByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDns64GroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDns64GroupByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDns64GroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDns64GroupByReference - errors', () => {
      it('should have a deleteDns64GroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDns64GroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteDns64GroupByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteDns64GroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridDnsByReference - errors', () => {
      it('should have a getGridDnsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getGridDnsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getGridDnsByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getGridDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGridDnsByReference - errors', () => {
      it('should have a createGridDnsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createGridDnsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.createGridDnsByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createGridDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGridDnsByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createGridDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGridDnsByReference - errors', () => {
      it('should have a updateGridDnsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateGridDnsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateGridDnsByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateGridDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGridDnsByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateGridDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostnameRewritePolicy - errors', () => {
      it('should have a getHostnameRewritePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getHostnameRewritePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostnameRewritePolicyByReference - errors', () => {
      it('should have a getHostnameRewritePolicyByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getHostnameRewritePolicyByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getHostnameRewritePolicyByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getHostnameRewritePolicyByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDnsByReference - errors', () => {
      it('should have a getMemberDnsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getMemberDnsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getMemberDnsByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getMemberDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMemberDnsByReference - errors', () => {
      it('should have a createMemberDnsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createMemberDnsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.createMemberDnsByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createMemberDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMemberDnsByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createMemberDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMemberDnsByReference - errors', () => {
      it('should have a updateMemberDnsByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateMemberDnsByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateMemberDnsByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateMemberDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMemberDnsByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateMemberDnsByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupByReference - errors', () => {
      it('should have a getNsGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsGroupByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNsGroupByReference - errors', () => {
      it('should have a updateNsGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNsGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNsGroupByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNsGroupByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupByReference - errors', () => {
      it('should have a deleteNsGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNsGroupByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNsGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupDelegation - errors', () => {
      it('should have a getNsGroupDelegation function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupDelegation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNsGroupDelegation - errors', () => {
      it('should have a createNsGroupDelegation function', (done) => {
        try {
          assert.equal(true, typeof a.createNsGroupDelegation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNsGroupDelegation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNsGroupDelegation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupDelegationByReference - errors', () => {
      it('should have a getNsGroupDelegationByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupDelegationByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsGroupDelegationByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsGroupDelegationByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNsGroupDelegationByReference - errors', () => {
      it('should have a updateNsGroupDelegationByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNsGroupDelegationByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNsGroupDelegationByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupDelegationByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNsGroupDelegationByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupDelegationByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupDelegationByReference - errors', () => {
      it('should have a deleteNsGroupDelegationByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsGroupDelegationByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNsGroupDelegationByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNsGroupDelegationByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardMember - errors', () => {
      it('should have a getNsGroupForwardMember function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupForwardMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNsGroupForwardMember - errors', () => {
      it('should have a createNsGroupForwardMember function', (done) => {
        try {
          assert.equal(true, typeof a.createNsGroupForwardMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNsGroupForwardMember('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNsGroupForwardMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardMemberByReference - errors', () => {
      it('should have a getNsGroupForwardMemberByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupForwardMemberByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsGroupForwardMemberByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsGroupForwardMemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNsGroupForwardMemberByReference - errors', () => {
      it('should have a updateNsGroupForwardMemberByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNsGroupForwardMemberByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNsGroupForwardMemberByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupForwardMemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNsGroupForwardMemberByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupForwardMemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupForwardMemberByReference - errors', () => {
      it('should have a deleteNsGroupForwardMemberByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsGroupForwardMemberByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNsGroupForwardMemberByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNsGroupForwardMemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardStubServer - errors', () => {
      it('should have a getNsGroupForwardStubServer function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupForwardStubServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNsGroupForwardStubServer - errors', () => {
      it('should have a createNsGroupForwardStubServer function', (done) => {
        try {
          assert.equal(true, typeof a.createNsGroupForwardStubServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNsGroupForwardStubServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNsGroupForwardStubServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardStubServerByReference - errors', () => {
      it('should have a getNsGroupForwardStubServerByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupForwardStubServerByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsGroupForwardStubServerByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsGroupForwardStubServerByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNsGroupForwardStubServerByReference - errors', () => {
      it('should have a updateNsGroupForwardStubServerByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNsGroupForwardStubServerByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNsGroupForwardStubServerByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupForwardStubServerByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNsGroupForwardStubServerByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupForwardStubServerByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupForwardStubServerByReference - errors', () => {
      it('should have a deleteNsGroupForwardStubServerByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsGroupForwardStubServerByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNsGroupForwardStubServerByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNsGroupForwardStubServerByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupStubmember - errors', () => {
      it('should have a getNsGroupStubmember function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupStubmember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNsGroupStubmember - errors', () => {
      it('should have a createNsGroupStubmember function', (done) => {
        try {
          assert.equal(true, typeof a.createNsGroupStubmember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNsGroupStubmember('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNsGroupStubmember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupStubmemberByReference - errors', () => {
      it('should have a getNsGroupStubmemberByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsGroupStubmemberByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsGroupStubmemberByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsGroupStubmemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNsGroupStubmemberByReference - errors', () => {
      it('should have a updateNsGroupStubmemberByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNsGroupStubmemberByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNsGroupStubmemberByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupStubmemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNsGroupStubmemberByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsGroupStubmemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupStubmemberByReference - errors', () => {
      it('should have a deleteNsGroupStubmemberByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsGroupStubmemberByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNsGroupStubmemberByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNsGroupStubmemberByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArecordByReference - errors', () => {
      it('should have a getArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getArecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAAArecordByReference - errors', () => {
      it('should have a getAAAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getAAAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getAAAArecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAAAArecordByReference - errors', () => {
      it('should have a updateAAAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateAAAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateAAAArecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAAAArecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAAAArecordByReference - errors', () => {
      it('should have a deleteAAAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAAAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteAAAArecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAliasrecord - errors', () => {
      it('should have a getAliasrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getAliasrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAliasrecord - errors', () => {
      it('should have a createAliasrecord function', (done) => {
        try {
          assert.equal(true, typeof a.createAliasrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAliasrecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createAliasrecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAliasrecordByReference - errors', () => {
      it('should have a getAliasrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getAliasrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getAliasrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getAliasrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAliasrecordByReference - errors', () => {
      it('should have a updateAliasrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateAliasrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateAliasrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateAliasrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAliasrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateAliasrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAliasrecordByReference - errors', () => {
      it('should have a deleteAliasrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAliasrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteAliasrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteAliasrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCAArecord - errors', () => {
      it('should have a getCAArecord function', (done) => {
        try {
          assert.equal(true, typeof a.getCAArecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCAArecord - errors', () => {
      it('should have a createCAArecord function', (done) => {
        try {
          assert.equal(true, typeof a.createCAArecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCAArecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createCAArecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCAArecordByReference - errors', () => {
      it('should have a getCAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getCAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getCAArecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getCAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCAArecordByReference - errors', () => {
      it('should have a updateCAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateCAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateCAArecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateCAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCAArecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateCAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCAArecordByReference - errors', () => {
      it('should have a deleteCAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteCAArecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteCAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCnamerecordByReference - errors', () => {
      it('should have a getCnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getCnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getCnamerecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getCnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcidrecord - errors', () => {
      it('should have a getDhcidrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcidrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcidrecordByReference - errors', () => {
      it('should have a getDhcidrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDhcidrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDhcidrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDhcidrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcidrecordByReference - errors', () => {
      it('should have a deleteDhcidrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcidrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteDhcidrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteDhcidrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnamerecord - errors', () => {
      it('should have a getDnamerecord function', (done) => {
        try {
          assert.equal(true, typeof a.getDnamerecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDnamerecord - errors', () => {
      it('should have a createDnamerecord function', (done) => {
        try {
          assert.equal(true, typeof a.createDnamerecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDnamerecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createDnamerecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnamerecordByReference - errors', () => {
      it('should have a getDnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDnamerecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDnamerecordByReference - errors', () => {
      it('should have a updateDnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateDnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateDnamerecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDnamerecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateDnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnamerecordByReference - errors', () => {
      it('should have a deleteDnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteDnamerecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteDnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnskeyrecord - errors', () => {
      it('should have a getDnskeyrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getDnskeyrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnskeyrecordByReference - errors', () => {
      it('should have a getDnskeyrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDnskeyrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDnskeyrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDnskeyrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDsrecord - errors', () => {
      it('should have a getDsrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getDsrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDsrecordByReference - errors', () => {
      it('should have a getDsrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDsrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDsrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDsrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDsrecordByReference - errors', () => {
      it('should have a deleteDsrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDsrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteDsrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteDsrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtclbdnrecord - errors', () => {
      it('should have a getDtclbdnrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getDtclbdnrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtclbdnrecordByReference - errors', () => {
      it('should have a getDtclbdnrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getDtclbdnrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getDtclbdnrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getDtclbdnrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv4Addrrecord - errors', () => {
      it('should have a getHostIpv4Addrrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getHostIpv4Addrrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv4AddrrecordByReference - errors', () => {
      it('should have a getHostIpv4AddrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getHostIpv4AddrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getHostIpv4AddrrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getHostIpv4AddrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHostIpv4AddrrecordByReference - errors', () => {
      it('should have a updateHostIpv4AddrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateHostIpv4AddrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateHostIpv4AddrrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateHostIpv4AddrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHostIpv4AddrrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateHostIpv4AddrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv6Addrrecord - errors', () => {
      it('should have a getHostIpv6Addrrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getHostIpv6Addrrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv6AddrrecordByReference - errors', () => {
      it('should have a getHostIpv6AddrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getHostIpv6AddrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getHostIpv6AddrrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getHostIpv6AddrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHostIpv6AddrrecordByReference - errors', () => {
      it('should have a updateHostIpv6AddrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateHostIpv6AddrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateHostIpv6AddrrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateHostIpv6AddrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHostIpv6AddrrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateHostIpv6AddrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMxrecordByReference - errors', () => {
      it('should have a getMxrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getMxrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getMxrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMxrecordByReference - errors', () => {
      it('should have a updateMxrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateMxrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateMxrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMxrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMxrecordByReference - errors', () => {
      it('should have a deleteMxrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMxrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteMxrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNaptrrecord - errors', () => {
      it('should have a getNaptrrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getNaptrrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNaptrrecord - errors', () => {
      it('should have a createNaptrrecord function', (done) => {
        try {
          assert.equal(true, typeof a.createNaptrrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNaptrrecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createNaptrrecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNaptrrecordByReference - errors', () => {
      it('should have a getNaptrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNaptrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNaptrrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNaptrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNaptrrecordByReference - errors', () => {
      it('should have a updateNaptrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNaptrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNaptrrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNaptrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNaptrrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNaptrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNaptrrecordByReference - errors', () => {
      it('should have a deleteNaptrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNaptrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNaptrrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNaptrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsrecordByReference - errors', () => {
      it('should have a getNsrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNsrecordByReference - errors', () => {
      it('should have a updateNsrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateNsrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateNsrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNsrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateNsrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsrecordByReference - errors', () => {
      it('should have a deleteNsrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNsrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteNsrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteNsrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsecrecord - errors', () => {
      it('should have a getNsecrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getNsecrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsecrecordByReference - errors', () => {
      it('should have a getNsecrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsecrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsecrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsecrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3record - errors', () => {
      it('should have a getNsec3record function', (done) => {
        try {
          assert.equal(true, typeof a.getNsec3record === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3recordByReference - errors', () => {
      it('should have a getNsec3recordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsec3recordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsec3recordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsec3recordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3paramrecord - errors', () => {
      it('should have a getNsec3paramrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getNsec3paramrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3paramrecordByReference - errors', () => {
      it('should have a getNsec3paramrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getNsec3paramrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getNsec3paramrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNsec3paramrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPtrrecordByReference - errors', () => {
      it('should have a getPtrrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getPtrrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getPtrrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getPtrrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRrsigrecord - errors', () => {
      it('should have a getRrsigrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getRrsigrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRrsigrecordByReference - errors', () => {
      it('should have a getRrsigrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getRrsigrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getRrsigrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getRrsigrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrvrecordByReference - errors', () => {
      it('should have a getSrvrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSrvrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSrvrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSrvrecordByReference - errors', () => {
      it('should have a updateSrvrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSrvrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSrvrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSrvrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSrvrecordByReference - errors', () => {
      it('should have a deleteSrvrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSrvrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSrvrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTlsarecord - errors', () => {
      it('should have a getTlsarecord function', (done) => {
        try {
          assert.equal(true, typeof a.getTlsarecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTlsarecord - errors', () => {
      it('should have a createTlsarecord function', (done) => {
        try {
          assert.equal(true, typeof a.createTlsarecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTlsarecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createTlsarecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTlsarecordByReference - errors', () => {
      it('should have a getTlsarecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getTlsarecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getTlsarecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getTlsarecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTlsarecordByReference - errors', () => {
      it('should have a updateTlsarecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateTlsarecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateTlsarecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateTlsarecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTlsarecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateTlsarecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTlsarecordByReference - errors', () => {
      it('should have a deleteTlsarecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTlsarecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteTlsarecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteTlsarecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTxtrecordByReference - errors', () => {
      it('should have a getTxtrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getTxtrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getTxtrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTxtrecordByReference - errors', () => {
      it('should have a updateTxtrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateTxtrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateTxtrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTxtrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTxtrecordByReference - errors', () => {
      it('should have a deleteTxtrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTxtrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteTxtrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownrecord - errors', () => {
      it('should have a getUnknownrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getUnknownrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUnknownrecord - errors', () => {
      it('should have a createUnknownrecord function', (done) => {
        try {
          assert.equal(true, typeof a.createUnknownrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUnknownrecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createUnknownrecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownrecordByReference - errors', () => {
      it('should have a getUnknownrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getUnknownrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getUnknownrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getUnknownrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUnknownrecordByReference - errors', () => {
      it('should have a updateUnknownrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateUnknownrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateUnknownrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateUnknownrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUnknownrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateUnknownrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnknownrecordByReference - errors', () => {
      it('should have a deleteUnknownrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnknownrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteUnknownrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteUnknownrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordNamePolicy - errors', () => {
      it('should have a getRecordNamePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getRecordNamePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRecordNamePolicy - errors', () => {
      it('should have a createRecordNamePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createRecordNamePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRecordNamePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createRecordNamePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordNamePolicyByReference - errors', () => {
      it('should have a getRecordNamePolicyByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getRecordNamePolicyByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getRecordNamePolicyByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getRecordNamePolicyByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRecordNamePolicyByReference - errors', () => {
      it('should have a updateRecordNamePolicyByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateRecordNamePolicyByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateRecordNamePolicyByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateRecordNamePolicyByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRecordNamePolicyByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateRecordNamePolicyByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRecordNamePolicyByReference - errors', () => {
      it('should have a deleteRecordNamePolicyByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRecordNamePolicyByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteRecordNamePolicyByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteRecordNamePolicyByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRuleset - errors', () => {
      it('should have a getRuleset function', (done) => {
        try {
          assert.equal(true, typeof a.getRuleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRuleset - errors', () => {
      it('should have a createRuleset function', (done) => {
        try {
          assert.equal(true, typeof a.createRuleset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRuleset('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createRuleset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRulesetByReference - errors', () => {
      it('should have a getRulesetByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getRulesetByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getRulesetByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getRulesetByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRulesetByReference - errors', () => {
      it('should have a updateRulesetByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateRulesetByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateRulesetByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateRulesetByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRulesetByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateRulesetByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRulesetByReference - errors', () => {
      it('should have a deleteRulesetByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRulesetByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteRulesetByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteRulesetByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScavengingTask - errors', () => {
      it('should have a getScavengingTask function', (done) => {
        try {
          assert.equal(true, typeof a.getScavengingTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScavengingTaskByReference - errors', () => {
      it('should have a getScavengingTaskByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getScavengingTaskByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getScavengingTaskByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getScavengingTaskByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedArecord - errors', () => {
      it('should have a getSharedArecord function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedArecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedArecord - errors', () => {
      it('should have a createSharedArecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedArecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedArecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedArecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedArecordByReference - errors', () => {
      it('should have a getSharedArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedArecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedArecordByReference - errors', () => {
      it('should have a updateSharedArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedArecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedArecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedArecordByReference - errors', () => {
      it('should have a deleteSharedArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedArecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedAAAArecord - errors', () => {
      it('should have a getSharedAAAArecord function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedAAAArecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedAAAArecord - errors', () => {
      it('should have a createSharedAAAArecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedAAAArecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedAAAArecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedAAAArecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedAAAArecordByReference - errors', () => {
      it('should have a getSharedAAAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedAAAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedAAAArecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedAAAArecordByReference - errors', () => {
      it('should have a updateSharedAAAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedAAAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedAAAArecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedAAAArecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedAAAArecordByReference - errors', () => {
      it('should have a deleteSharedAAAArecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedAAAArecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedAAAArecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedAAAArecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedCnamerecord - errors', () => {
      it('should have a getSharedCnamerecord function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedCnamerecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedCnamerecord - errors', () => {
      it('should have a createSharedCnamerecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedCnamerecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedCnamerecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedCnamerecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedCnamerecordByReference - errors', () => {
      it('should have a getSharedCnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedCnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedCnamerecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedCnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedCnamerecordByReference - errors', () => {
      it('should have a updateSharedCnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedCnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedCnamerecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedCnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedCnamerecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedCnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedCnamerecordByReference - errors', () => {
      it('should have a deleteSharedCnamerecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedCnamerecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedCnamerecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedCnamerecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedMxrecord - errors', () => {
      it('should have a getSharedMxrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedMxrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedMxrecord - errors', () => {
      it('should have a createSharedMxrecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedMxrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedMxrecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedMxrecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedMxrecordByReference - errors', () => {
      it('should have a getSharedMxrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedMxrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedMxrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedMxrecordByReference - errors', () => {
      it('should have a updateSharedMxrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedMxrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedMxrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedMxrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedMxrecordByReference - errors', () => {
      it('should have a deleteSharedMxrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedMxrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedMxrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedMxrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedSrvrecord - errors', () => {
      it('should have a getSharedSrvrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedSrvrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedSrvrecord - errors', () => {
      it('should have a createSharedSrvrecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedSrvrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedSrvrecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedSrvrecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedSrvrecordByReference - errors', () => {
      it('should have a getSharedSrvrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedSrvrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedSrvrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedSrvrecordByReference - errors', () => {
      it('should have a updateSharedSrvrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedSrvrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedSrvrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedSrvrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedSrvrecordByReference - errors', () => {
      it('should have a deleteSharedSrvrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedSrvrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedSrvrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedSrvrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedTxtrecord - errors', () => {
      it('should have a getSharedTxtrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedTxtrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedTxtrecord - errors', () => {
      it('should have a createSharedTxtrecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedTxtrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedTxtrecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedTxtrecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedTxtrecordByReference - errors', () => {
      it('should have a getSharedTxtrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedTxtrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedTxtrecordByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedTxtrecordByReference - errors', () => {
      it('should have a updateSharedTxtrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedTxtrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedTxtrecordByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedTxtrecordByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedTxtrecordByReference - errors', () => {
      it('should have a deleteSharedTxtrecordByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedTxtrecordByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedTxtrecordByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedTxtrecordByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedRecordGroup - errors', () => {
      it('should have a getSharedRecordGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedRecordGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSharedRecordGroup - errors', () => {
      it('should have a createSharedRecordGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createSharedRecordGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSharedRecordGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createSharedRecordGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedRecordGroupByReference - errors', () => {
      it('should have a getSharedRecordGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getSharedRecordGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getSharedRecordGroupByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getSharedRecordGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSharedRecordGroupByReference - errors', () => {
      it('should have a updateSharedRecordGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateSharedRecordGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateSharedRecordGroupByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedRecordGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSharedRecordGroupByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateSharedRecordGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedRecordGroupByReference - errors', () => {
      it('should have a deleteSharedRecordGroupByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSharedRecordGroupByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteSharedRecordGroupByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteSharedRecordGroupByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createView - errors', () => {
      it('should have a createView function', (done) => {
        try {
          assert.equal(true, typeof a.createView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createView('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewByReference - errors', () => {
      it('should have a getViewByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getViewByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getViewByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getViewByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createViewByReference - errors', () => {
      it('should have a createViewByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createViewByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.createViewByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createViewByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createViewByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createViewByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateViewByReference - errors', () => {
      it('should have a updateViewByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateViewByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateViewByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateViewByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateViewByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateViewByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteViewByReference - errors', () => {
      it('should have a deleteViewByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteViewByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteViewByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteViewByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneAuth - errors', () => {
      it('should have a getZoneAuth function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneAuth - errors', () => {
      it('should have a createZoneAuth function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneAuth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateZoneAuthByReference - errors', () => {
      it('should have a updateZoneAuthByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateZoneAuthByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateZoneAuthByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneAuthByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateZoneAuthByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneAuthByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneAuthDiscrepancy - errors', () => {
      it('should have a getZoneAuthDiscrepancy function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneAuthDiscrepancy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneAuthDiscrepancyByReference - errors', () => {
      it('should have a getZoneAuthDiscrepancyByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneAuthDiscrepancyByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getZoneAuthDiscrepancyByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getZoneAuthDiscrepancyByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneDelegatedByReference - errors', () => {
      it('should have a getZoneDelegatedByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneDelegatedByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getZoneDelegatedByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getZoneDelegatedByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneDelegatedByReference - errors', () => {
      it('should have a createZoneDelegatedByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneDelegatedByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.createZoneDelegatedByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneDelegatedByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneDelegatedByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneDelegatedByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateZoneDelegatedByReference - errors', () => {
      it('should have a updateZoneDelegatedByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateZoneDelegatedByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateZoneDelegatedByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneDelegatedByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateZoneDelegatedByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneDelegatedByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneDelegatedByReference - errors', () => {
      it('should have a deleteZoneDelegatedByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZoneDelegatedByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteZoneDelegatedByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteZoneDelegatedByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneForwardByReference - errors', () => {
      it('should have a getZoneForwardByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneForwardByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.getZoneForwardByReference(null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getZoneForwardByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneForwardByReference - errors', () => {
      it('should have a createZoneForwardByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneForwardByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.createZoneForwardByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneForwardByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneForwardByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneForwardByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateZoneForwardByReference - errors', () => {
      it('should have a updateZoneForwardByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateZoneForwardByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.updateZoneForwardByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneForwardByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateZoneForwardByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneForwardByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneForwardByReference - errors', () => {
      it('should have a deleteZoneForwardByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZoneForwardByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing refInfo', (done) => {
        try {
          a.deleteZoneForwardByReference(null, (data, error) => {
            try {
              const displayE = 'refInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteZoneForwardByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneRpByReference - errors', () => {
      it('should have a getZoneRpByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneRpByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneRpReference', (done) => {
        try {
          a.getZoneRpByReference(null, null, (data, error) => {
            try {
              const displayE = 'zoneRpReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getZoneRpByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneRpByReference - errors', () => {
      it('should have a createZoneRpByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneRpByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneRpReference', (done) => {
        try {
          a.createZoneRpByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'zoneRpReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneRpByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneRpByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneRpByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateZoneRpByReference - errors', () => {
      it('should have a updateZoneRpByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateZoneRpByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneRpReference', (done) => {
        try {
          a.updateZoneRpByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'zoneRpReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneRpByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateZoneRpByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneRpByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneRpByReference - errors', () => {
      it('should have a deleteZoneRpByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZoneRpByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneRpReference', (done) => {
        try {
          a.deleteZoneRpByReference(null, (data, error) => {
            try {
              const displayE = 'zoneRpReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteZoneRpByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneStub - errors', () => {
      it('should have a getZoneStub function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneStub - errors', () => {
      it('should have a createZoneStub function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneStub === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneStub('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneStub', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneStubByReference - errors', () => {
      it('should have a getZoneStubByReference function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneStubByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneStubReference', (done) => {
        try {
          a.getZoneStubByReference(null, null, (data, error) => {
            try {
              const displayE = 'zoneStubReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getZoneStubByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneStubByReference - errors', () => {
      it('should have a createZoneStubByReference function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneStubByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneStubReference', (done) => {
        try {
          a.createZoneStubByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'zoneStubReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneStubByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneStubByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-createZoneStubByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateZoneStubByReference - errors', () => {
      it('should have a updateZoneStubByReference function', (done) => {
        try {
          assert.equal(true, typeof a.updateZoneStubByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneStubReference', (done) => {
        try {
          a.updateZoneStubByReference(null, null, null, (data, error) => {
            try {
              const displayE = 'zoneStubReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneStubByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateZoneStubByReference('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-updateZoneStubByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneStubByReference - errors', () => {
      it('should have a deleteZoneStubByReference function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZoneStubByReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneStubReference', (done) => {
        try {
          a.deleteZoneStubByReference(null, (data, error) => {
            try {
              const displayE = 'zoneStubReference is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-deleteZoneStubByReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkZoneAssociations - errors', () => {
      it('should have a getNetworkZoneAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkZoneAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkZoneAssociations(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkZoneAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId subnet mask', (done) => {
        try {
          a.getNetworkZoneAssociations('fakedata', null, null, (data, error) => {
            try {
              const displayE = 'subnet mask in networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-getNetworkZoneAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkZoneAssociation - errors', () => {
      it('should have a addNetworkZoneAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkZoneAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addNetworkZoneAssociation(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addNetworkZoneAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId subnet mask', (done) => {
        try {
          a.addNetworkZoneAssociation('fakedata', null, null, null, (data, error) => {
            try {
              const displayE = 'subnet mask in networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addNetworkZoneAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addNetworkZoneAssociation('fakedata/16', null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-infoblox-adapter-addNetworkZoneAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
