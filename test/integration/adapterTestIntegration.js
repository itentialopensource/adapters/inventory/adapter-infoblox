/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-infoblox',
      type: 'Infoblox',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Infoblox = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Infoblox Adapter Test', () => {
  describe('Infoblox Class Tests', () => {
    const a = new Infoblox(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const testParams = {};
    testParams.network1 = '1.1.1.0/24';
    testParams.ip1 = '1.1.1.0';
    testParams.network1Ref = 'ZG5zLm5ldHdvcmskMS4xLjEuMC8zMC8w:1.1.1.0/30/default';
    testParams.nextNetworkSubnetMask = '30';
    testParams.comment = 'Name of the network';
    testParams.fqdnName = 'test.infoblox.com';
    testParams.hostName = 'itential';
    testParams.hostnameFqdnName = `${testParams.hostName}.${testParams.fqdnName}`;
    testParams.hostName2 = 'itential2';
    testParams.network3Ref = 'network/ZG5zLm5ldHdvcmskMi4xLjEyLjAvMzAvMA:2.1.12.0/30/default';

    describe('#createNetwork', () => {
      it('should create a network or give duplicate error', (done) => {
        try {
          a.createNetwork(testParams.network1, testParams.comment, (data, error) => {
            try {
              if (stub) {
                assert.equal('network/ZG5zLm5ldHdvcmskMi4xLjEyLjAvMzAvMA:2.1.12.0/30/default', data.response.id);
              } else if (!data) {
                // Handling duplicate network error to pass integration test and move on.
                log.warn('Network already exists.');
                assert.equal(undefined, data);
              } else {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.id);
                assert.equal(data.response.code, 'Client.Ibap.Data.Conflict');
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);

      it('should error if duplicate network.', (done) => {
        try {
          a.createNetwork(testParams.network1, testParams.comment, (data, error) => {
            try {
              if (stub) {
                console.warn('This does not actually test in stub mode');
              } else {
                assert.equal(undefined, data);
                assert.equal(JSON.parse(error.IAPerror.raw_response.response).code, 'Client.Ibap.Data.Conflict');
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /* network calls */

    describe('#assignNextNetwork', () => {
      it('should get the next available network', (done) => {
        try {
          a.assignNextNetwork(testParams.network1, testParams.nextNetworkSubnetMask, testParams.comment, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(
                  'network/ZG5zLm5ldHdvcmskNy43LjcuMTA0LzMwLzA:7.7.7.104/30/default',
                  data.response._ref
                );
                assert.equal('7.7.7.104/30', data.response.network);
              } else {
                assert.notEqual(null, data.response._ref);
                testParams.network2 = data.response.network;
                testParams.network2Ref = data.response._ref;
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextNetworkByNetwork', () => {
      it('should get the next available network', (done) => {
        try {
          a.assignNextNetworkByNetwork(testParams.network1, testParams.nextNetworkSubnetMask, null, null, testParams.comment, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(
                  'network/ZG5zLm5ldHdvcmskNy43LjcuMTA0LzMwLzA:7.7.7.104/30/default',
                  data.response._ref
                );
                assert.equal('7.7.7.104/30', data.response.network);
              } else {
                assert.notEqual(null, data.response._ref);
                testParams.network2 = data.response.network;
                testParams.network2Ref = data.response._ref;
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextNetworkByRef', () => {
      it('should get the next available network', (done) => {
        try {
          a.assignNextNetworkByRef(testParams.network1Ref, testParams.nextNetworkSubnetMask, null, null, testParams.comment, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(
                  'network/ZG5zLm5ldHdvcmskNy43LjcuMTA0LzMwLzA:7.7.7.104/30/default',
                  data.response._ref
                );
                assert.equal('7.7.7.104/30', data.response.network);
              } else {
                assert.notEqual(null, data.response._ref);
                testParams.network2 = data.response.network;
                testParams.network2Ref = data.response._ref;
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkKeyByIP', () => {
      it('should get network keys by IP', (done) => {
        try {
          a.getNetworkKeyByIP(testParams.network1, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('network/ZG5zLm5ldHdvcmskMS4xLjEuMC8zMC8w:1.1.1.0/30/default', data.response[0]._ref);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpDetails', () => {
      it('should get details of IP Address', (done) => {
        try {
          a.getIpDetails((testParams.network1.match(/(.*?)(?=\/)/))[0], (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('ipv4address/Li5pcHY0X2FkZHJlc3MkMS4xLjEuMS8w:1.1.1.1', data.response[0]._ref);
                assert.equal('1.1.1.1', data.response[0].ip_address);
                assert.equal('UNUSED', data.response[0].status);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkKeyByIP', () => {
      it('should get network key of Network IP', (done) => {
        try {
          a.getNetworkKeyByIP(testParams.network1, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('network/ZG5zLm5ldHdvcmskMS4xLjEuMC8zMC8w:1.1.1.0/30/default', data.response[0]._ref);
                assert.equal('1.1.1.0/30', data.response[0].network);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkUtilizationByIP', () => {
      it('should get network utilization by network IP', (done) => {
        try {
          a.getNetworkUtilizationByIP(testParams.network1, (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);

              if (stub) {
                assert.equal('ipam:statistics/ZG5zLm5ldHdvcmtfY29udGFpbmVyJDEuMS4xLjAvMjQvMA:default/1.1.1.0/24', data._ref);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableIp', () => {
      it('should get the next available IP from a network block', (done) => {
        try {
          a.getNextAvailableIp(testParams.network1Ref, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('172.21.10.1', data.response.ips[0]);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableIps', () => {
      it('should get N number of next available IPs from a network block', (done) => {
        try {
          a.getNextAvailableIps(testParams.network1Ref, 2, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(null, data.response.ips);
                assert.equal(2, data.response.ips.length);
              } else {
                runCommonAsserts(data, error);
                assert.notEqual(null, data.response.ips);
                assert.notEqual(0, data.response.ips.length);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableNetworks', () => {
      it('should get next available network', (done) => {
        try {
          a.getNextAvailableNetworks(testParams.network1Ref, testParams.nextNetworkSubnetMask, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(2, data.response.networks.length);
              } else {
                assert.notEqual(null, data.response.network);
                assert.notEqual(0, data.response.network.length);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkByNetworkKey', () => {
      it('it should delete a network by network key', (done) => {
        try {
          a.deleteNetworkByNetworkKey(stub ? testParams.network3Ref : testParams.network2Ref, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                // testParams.network2Ref should be the first one created on first run.
                assert.equal(testParams.network3Ref, data.response.result);
              } else {
                assert.notEqual(null, data.response.result);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /* zone_auth calls */

    describe('#createAuthZone', () => {
      it('it should create fqdnName or error if duplicate fqdnName', (done) => {
        try {
          a.createAuthZone(testParams.fqdnName, (data, error) => {
            try {
              if (stub) {
                assert.equal('zone_auth/ZG5zLnpvbmUkLl9kZWZhdWx0LmNvbS5pbmZvLm15:my.info.com/default', data.response.result);
              } else if (!data) {
                // Handling duplicate network error to pass integration test and move on.
                log.warn('fqdnName already exists.');
                assert.equal(undefined, data);
                assert.equal(JSON.parse(error.IAPerror.raw_response.response).code, 'Client.Ibap.Data.Conflict');
              } else {
                runCommonAsserts(data, error);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);

      it('should error if duplicate fqdnName.', (done) => {
        try {
          a.createNetwork(testParams.network1, testParams.comment, (data, error) => {
            try {
              if (stub) {
                console.warn('This does not actually test in stub mode');
              } else {
                assert.equal(undefined, data);
                assert.equal(JSON.parse(error.IAPerror.raw_response.response).code, 'Client.Ibap.Data.Conflict');
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /* recordHost calls */

    describe('#createHostRecord', () => {
      it('should create a HostRecord or error if duplicate HostRecord', (done) => {
        try {
          a.createHostRecord(testParams.hostName, testParams.fqdnName, testParams.ip1, (data, error) => {
            try {
              if (stub) {
                assert.equal('record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS50ZXN0Lml0ZW50aWFsLmFua2l0Mg:ankit2.itential.test.com/default', data.response.result);
              } else if (!data) {
                // Handling duplicate network error to pass integration test and move on.
                log.warn('HostRecord already exists.');
                assert.equal(undefined, data);
                // assert.equal(JSON.parse(error.IAPerror.raw_response.response).code, 'Client.Ibap.Data.Conflict');
              } else {
                runCommonAsserts(data, error);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);

      it('should error if duplicate HostRecord.', (done) => {
        try {
          a.createNetwork(testParams.network1, testParams.comment, (data, error) => {
            try {
              if (stub) {
                console.warn('This does not actually test in stub mode');
              } else {
                assert.equal(undefined, data);
                assert.equal(JSON.parse(error.IAPerror.raw_response.response).code, 'Client.Ibap.Data.Conflict');
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostKeyByHostName', () => {
      it('should get HostKey by hostName', (done) => {
        try {
          a.getHostKeyByHostName(testParams.hostnameFqdnName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS50ZXN0Lml0ZW50aWFsLmFua2l0MjI:ankit22.itential.test.com/default', data.response[0]._ref);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpByHost', () => {
      it('should get IP by hostName', (done) => {
        try {
          a.getIpByHost(testParams.hostnameFqdnName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('1.1.1.244', data.response[0].ipv4addrs[0].ipv4addr);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextIP', () => {
      it('should create an IP', (done) => {
        try {
          a.assignNextIP(testParams.ip1, testParams.hostName2, testParams.fqdnName, (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);

              if (stub) {
                // TODO: Create mock data to test in stub mode
                log.warn('No stub mode test being run for assignNextIP.');
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostRecordByHostName', () => {
      it('should delete hostRecord by hostName', (done) => {
        try {
          a.deleteHostRecordByHostName(testParams.hostnameFqdnName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS50ZXN0Lml0ZW50aWFsLmFua2l0MjI:ankit22.itential.test.com/default', data.response.result);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    // unassgnIP needs to run before deleting the hostRecord, otherwise deleting hostRecord fails.

    describe('#unassignIP', () => {
      it('should unassign the registered IP', (done) => {
        try {
          a.unassignIP(testParams.ip1, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS50ZXN0Lml0ZW50aWFsLmFua2l0MjI:ankit22.itential.test.com/default', data.response.result);
              } else {
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesCreateZoneDelegationBodyParam = {
      fqdn: 'string',
      delegate_to: [
        {
          address: '10.10.10.20',
          name: 'host.demo.info.com'
        }
      ]
    };

    describe('#createZoneDelegation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createZoneDelegation(zonesCreateZoneDelegationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'createZoneDelegation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesCreateForwardZoneBodyParam = {
      fqdn: 'string',
      forward_to: [
        {
          address: '10.10.10.11',
          name: 'host.infoblox.com'
        }
      ]
    };

    describe('#createForwardZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createForwardZone(zonesCreateForwardZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'createForwardZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesFqdn = 'fakedata';

    describe('#getAuthZoneDetailsByfqdnName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuthZoneDetailsByfqdnName(zonesFqdn, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getAuthZoneDetailsByfqdnName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneDelegations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneDelegations((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getZoneDelegations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getForwardZones - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getForwardZones((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'getForwardZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksNetworkRef = 'fakedata';
    const networksOptions = 'fakedata';
    const networksGenericCreateNextAvailableNetworkBodyParam = {};

    describe('#genericCreateNextAvailableNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.genericCreateNextAvailableNetwork(networksNetworkRef, networksOptions, networksGenericCreateNextAvailableNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'genericCreateNextAvailableNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksCreateNetworkContainerBodyParam = {};

    describe('#createNetworkContainer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkContainer(networksCreateNetworkContainerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'createNetworkContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksNetwork = 'fakedata';

    describe('#getNetworkDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkDetails(networksNetwork, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getNetworkDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContainerDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkContainerDetails(networksNetwork, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getNetworkContainerDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('#listNetworkContainers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listNetworkContainers({ key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'listNetworkContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksObjectReference = 'fakedata';

    describe('#modifyNetworkBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyNetworkBlock(networksObjectReference, networksObjectReference, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'modifyNetworkBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkBlock(networksObjectReference, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getNetworkBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContainerNextNetworkIps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkContainerNextNetworkIps('fakedata', 'fakedata', { key: 'fakedata' }, { key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getNetworkContainerNextNetworkIps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpv6NetworkContainerNextNetworkIps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIpv6NetworkContainerNextNetworkIps('fakedata', 'fakedata', { key: 'fakedata' }, { key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getIpv6NetworkContainerNextNetworkIps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSTrafficControlCreateDtcLbdnBodyParam = {
      auth_zones: [
        'string'
      ],
      lb_method: 'string',
      name: 'string',
      patterns: [
        'string'
      ],
      pools: [
        {
          ratio: 4,
          pool: '{{object_reference_of_DTC_pool}}'
        }
      ]
    };

    describe('#createDtcLbdn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDtcLbdn(dNSTrafficControlCreateDtcLbdnBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSTrafficControl', 'createDtcLbdn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSTrafficControlCreateDtcPoolBodyParam = {
      name: 'string',
      availability: 'string',
      lb_preferred_method: 'string',
      monitors: [
        'string'
      ],
      servers: [
        {
          ratio: 4,
          server: '{{object_reference_of_DTC_server}}'
        }
      ]
    };

    describe('#createDtcPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDtcPool(dNSTrafficControlCreateDtcPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSTrafficControl', 'createDtcPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSTrafficControlCreateDtcServerBodyParam = {
      name: 'string',
      host: 'string'
    };

    describe('#createDtcServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDtcServer(dNSTrafficControlCreateDtcServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSTrafficControl', 'createDtcServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSTrafficControlFunction = 'fakedata';
    const dNSTrafficControlObjectReference = 'fakedata';
    const dNSTrafficControlCreateNetworkBlockBodyParam = {
      service_option: 'string'
    };

    describe('#createNetworkBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkBlock(dNSTrafficControlFunction, dNSTrafficControlObjectReference, dNSTrafficControlCreateNetworkBlockBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSTrafficControl', 'createNetworkBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtcPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDtcPool((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSTrafficControl', 'getDtcPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtcServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDtcServer((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSTrafficControl', 'getDtcServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateARecordBodyParam = {
      name: 'string',
      ipv4addr: 'string'
    };

    describe('#createARecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createARecord(recordsCreateARecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createARecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateAaaaRecordBodyParam = {
      name: 'string',
      ipv6addr: 'string'
    };

    describe('#createAaaaRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAaaaRecord(recordsCreateAaaaRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createAaaaRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateCNAMERecordBodyParam = {
      name: 'string',
      canonical: 'string'
    };

    describe('#createCNAMERecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCNAMERecord(recordsCreateCNAMERecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createCNAMERecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateMxRecordBodyParam = {
      mail_exchanger: 'string',
      name: 'string',
      preference: 2
    };

    describe('#createMxRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createMxRecord(recordsCreateMxRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createMxRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateNsRecordBodyParam = {
      name: 'string',
      nameserver: 'string',
      addresses: [
        {
          address: '192.168.11.0'
        }
      ]
    };

    describe('#createNsRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNsRecord(recordsCreateNsRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createNsRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreatePtrRecordBodyParam = {
      name: 'string',
      ptrdname: 'string',
      ipv4addr: 'string'
    };

    describe('#createPtrRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPtrRecord(recordsCreatePtrRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createPtrRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateSrvRecordBodyParam = {
      name: 'string',
      port: 2,
      priority: 7,
      target: 'string',
      weight: 1
    };

    describe('#createSrvRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSrvRecord(recordsCreateSrvRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createSrvRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsCreateTxtRecordBodyParam = {
      name: 'string',
      text: 'string'
    };

    describe('#createTxtRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTxtRecord(recordsCreateTxtRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'createTxtRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsZone = 'fakedata';

    describe('#getAllRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllRecords(recordsZone, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getAllRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getARecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getARecords(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getARecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsRecordkey = 'fakedata';
    const recordsUpdateARecordBodyParam = {
      name: 'string',
      ipv4addr: 'string'
    };

    describe('#updateARecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateARecord(recordsRecordkey, recordsUpdateARecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'updateARecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaaRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAaaaRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getAaaaRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCnameRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCnameRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getCnameRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsUpdateCNAMERecordBodyParam = {
      name: 'string',
      canonical: 'string'
    };

    describe('#updateCNAMERecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCNAMERecord(recordsRecordkey, recordsUpdateCNAMERecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'updateCNAMERecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsHostName = 'fakedata';

    describe('#getHostRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostRecord(recordsHostName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getHostRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsUpdateHostRecordBodyParam = {
      name: 'string',
      ipv4addrs: [
        {
          ipv4addr: '10.10.10.20'
        }
      ]
    };

    describe('#updateHostRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateHostRecord(recordsRecordkey, recordsUpdateHostRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'updateHostRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMxRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMxRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getMxRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getNsRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPtrRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPtrRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getPtrRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const recordsUpdatePTRRecordBodyParam = {
      name: 'string',
      ptrdname: 'string',
      ipv4addr: 'string'
    };

    describe('#updatePTRRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePTRRecord(recordsRecordkey, recordsUpdatePTRRecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'updatePTRRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrvRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSrvRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getSrvRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTxtRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTxtRecords((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'getTxtRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridDns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGridDns((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSProperties', 'getGridDns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMemberDns((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNSProperties', 'getMemberDns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesCreateSubstituitionRuleForARecordsBodyParam = {
      name: 'string',
      ipv4addr: 'string',
      rp_zone: 'string'
    };

    describe('#createSubstituitionRuleForARecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSubstituitionRuleForARecords(responsePolicyZonesCreateSubstituitionRuleForARecordsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'createSubstituitionRuleForARecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddSubstituitionRuleForIpTriggerPolicyBodyParam = {
      name: 'string',
      ipv4addr: 'string',
      rp_zone: 'string'
    };

    describe('#addSubstituitionRuleForIpTriggerPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSubstituitionRuleForIpTriggerPolicy(responsePolicyZonesAddSubstituitionRuleForIpTriggerPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addSubstituitionRuleForIpTriggerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddBlockDomainNameRuleBodyParam = {
      name: 'string',
      canonical: 'string',
      rp_zone: 'string'
    };

    describe('#addBlockDomainNameRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addBlockDomainNameRule(responsePolicyZonesAddBlockDomainNameRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addBlockDomainNameRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddBlockClientIpAddressRuleBodyParam = {
      name: 'string',
      canonical: 'string',
      rp_zone: 'string'
    };

    describe('#addBlockClientIpAddressRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addBlockClientIpAddressRule(responsePolicyZonesAddBlockClientIpAddressRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addBlockClientIpAddressRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddSubstituteDomainNameClientIpAddressRuleBodyParam = {
      name: 'string',
      canonical: 'string',
      rp_zone: 'string'
    };

    describe('#addSubstituteDomainNameClientIpAddressRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSubstituteDomainNameClientIpAddressRule(responsePolicyZonesAddSubstituteDomainNameClientIpAddressRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addSubstituteDomainNameClientIpAddressRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddBlockIpAddressNoSuchDomainRuleBodyParam = {
      name: 'string',
      canonical: 'string',
      rp_zone: 'string'
    };

    describe('#addBlockIpAddressNoSuchDomainRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addBlockIpAddressNoSuchDomainRule(responsePolicyZonesAddBlockIpAddressNoSuchDomainRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addBlockIpAddressNoSuchDomainRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddSubstituteDomainNameIpAddressRuleBodyParam = {
      name: 'string',
      canonical: 'string',
      rp_zone: 'string'
    };

    describe('#addSubstituteDomainNameIpAddressRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSubstituteDomainNameIpAddressRule(responsePolicyZonesAddSubstituteDomainNameIpAddressRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addSubstituteDomainNameIpAddressRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesAddSubstituitionRuleForPtrRecordsBodyParam = {
      name: 'string',
      ipv4addr: 'string',
      ptrdname: 'string',
      rp_zone: 'string'
    };

    describe('#addSubstituitionRuleForPtrRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSubstituitionRuleForPtrRecords(responsePolicyZonesAddSubstituitionRuleForPtrRecordsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'addSubstituitionRuleForPtrRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesCreateResponsePolicyZoneBodyParam = {
      fqdn: 'string',
      rpz_policy: 'string',
      rpz_severity: 'string',
      substitute_name: 'string'
    };

    describe('#createResponsePolicyZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createResponsePolicyZone(responsePolicyZonesCreateResponsePolicyZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'createResponsePolicyZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const responsePolicyZonesZone = 'fakedata';

    describe('#getAllRpzRecords - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllRpzRecords(responsePolicyZonesZone, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'getAllRpzRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResponsePolicyZones - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getResponsePolicyZones((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ResponsePolicyZones', 'getResponsePolicyZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nameServerGroupsCreateNameServerGroupBodyParam = {
      name: 'string',
      grid_primary: [
        {
          name: 'infoblox.localdomain'
        }
      ]
    };

    describe('#createNameServerGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNameServerGroup(nameServerGroupsCreateNameServerGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NameServerGroups', 'createNameServerGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNameServerGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNameServerGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NameServerGroups', 'getNameServerGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rangesCreateRangeBodyParam = {
      start_addr: 'string',
      end_addr: 'string',
      server_association_type: 'string',
      member: {
        _struct: 'dhcpmember',
        ipv4addr: '172.26.1.3'
      }
    };

    describe('#createRange - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRange(rangesCreateRangeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ranges', 'createRange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rangesLocation = 'fakedata';

    describe('#getRangeByExtensibleAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRangeByExtensibleAttribute(rangesLocation, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ranges', 'getRangeByExtensibleAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const leasesAddress = 'fakedata';

    describe('#getLeaseByIpAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLeaseByIpAddress(leasesAddress, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Leases', 'getLeaseByIpAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDhcp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMemberDhcp((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCPProperties', 'getMemberDhcp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const iPv4AddressesAddress = 'fakedata';

    describe('#getIpAddressUsingSearch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIpAddressUsingSearch(iPv4AddressesAddress, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IPv4Addresses', 'getIpAddressUsingSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkViewsAndDNSViewsCreateNetworkViewBodyParam = {
      name: 'string'
    };

    describe('#createNetworkView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkView(networkViewsAndDNSViewsCreateNetworkViewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'createNetworkView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkView((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'getNetworkView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkViewWithQuery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkViewWithQuery({ key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'getNetworkView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkViewById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkViewById('fakedata', { key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'getNetworkViewById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkView('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'updateNetworkView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnsView((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'getDnsView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fixedAddressesCreateFixedAddressBodyParam = {
      ipv4addr: 'string',
      mac: 'string'
    };

    describe('#createFixedAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createFixedAddress(fixedAddressesCreateFixedAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FixedAddresses', 'createFixedAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fixedAddressesMac = 'fakedata';

    describe('#getFixedAddressMac - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFixedAddressMac(fixedAddressesMac, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FixedAddresses', 'getFixedAddressMac', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const membersCreateMemberBodyParam = {
      config_addr_type: 'string',
      platform: 'string',
      host_name: 'string',
      vip_setting: {
        subnet_mask: '255.255.255.0',
        address: '192.128.2.7',
        gateway: '192.128.2.1'
      }
    };

    describe('#createMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createMember(membersCreateMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Members', 'createMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMembers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Members', 'getMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGrid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGrid((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getGrid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGridStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getGridStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridPendingChanges - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGridPendingChanges((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getGridPendingChanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vDiscoveryCreateVdiscoveryTaskBodyParam = {
      name: 'string',
      driver_type: 'string',
      fqdn_or_ip: 'string',
      username: 'string',
      password: 'string',
      member: 'string',
      port: 7,
      protocol: 'string',
      auto_consolidate_cloud_ea: true,
      auto_consolidate_managed_tenant: true,
      auto_consolidate_managed_vm: false,
      merge_data: false,
      private_network_view: 'string',
      private_network_view_mapping_policy: 'string',
      public_network_view: 'string',
      public_network_view_mapping_policy: 'string',
      update_metadata: false,
      scheduled_run: {
        _struct: 'setting:schedule',
        frequency: 'HOURLY',
        repeat: 'RECUR',
        minutes_past_hour: 1,
        disable: true
      }
    };

    describe('#createVdiscoveryTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createVdiscoveryTask(vDiscoveryCreateVdiscoveryTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDiscovery', 'createVdiscoveryTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVdiscoveryTasks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVdiscoveryTasks((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VDiscovery', 'getVdiscoveryTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsRole = 'fakedata';

    describe('#getPermissionsForARole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPermissionsForARole(permissionsRole, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getPermissionsForARole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schemaSchema = 'fakedata';

    describe('#getWapiSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWapiSchema(schemaSchema, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schema', 'getWapiSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const combineMultipleWAPICallsUsingRequestMultipleRecordTypesBodyParam = [
      {
        method: 'GET',
        object: 'record:host',
        data: {},
        args: {
          _return_fields: 'name,dns_name,aliases,dns_aliases,ipv4addrs,configure_for_dns'
        }
      }
    ];

    describe('#multipleRecordTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.multipleRecordTypes(combineMultipleWAPICallsUsingRequestMultipleRecordTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CombineMultipleWAPICallsUsingRequest', 'multipleRecordTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extensibleAttributesCreateExtensibleAttributeDefinitionBodyParam = {
      name: 'string',
      type: 'string'
    };

    describe('#createExtensibleAttributeDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createExtensibleAttributeDefinition(extensibleAttributesCreateExtensibleAttributeDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensibleAttributes', 'createExtensibleAttributeDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtensibleAttributeDefinition((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensibleAttributes', 'getExtensibleAttributeDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinitionWithQuery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtensibleAttributeDefinitionWithQuery({ key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensibleAttributes', 'getExtensibleAttributeDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinitionById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtensibleAttributeDefinitionById('fakedata', { key: 'fakedata' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensibleAttributes', 'getExtensibleAttributeDefinitionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExtensibleAttributeDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExtensibleAttributeDefinition('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensibleAttributes', 'updateExtensibleAttributeDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const urlFromUploadInitiation = 'https://1.1.1.1/http_direct_file_io/req_id-UPLOAD-1111111111111111/import_records';
    const fileData = 'filedata';

    describe('#uploadFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadFile(urlFromUploadInitiation, fileData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileOps', 'uploadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const urlFromDownloadInitiation = 'https://1.1.1.1/http_direct_file_io/req_id-DOWNLOAD-111/csv-error.1.csv';

    describe('#downloadFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadFile(urlFromDownloadInitiation, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileOps', 'downloadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesZoneRef = 'fakedata';

    describe('#deleteAuthZoneByRef - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAuthZoneByRef(zonesZoneRef, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'deleteAuthZoneByRef', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesObjectReference = 'fakedata';

    describe('#deleteZones - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZones(zonesObjectReference, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'deleteZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetwork(networksNetworkRef, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'deleteNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkv2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkv2(networksNetworkRef, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'deleteNetworkv2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkContainer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkContainer(networksNetworkRef, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'deleteNetworkContainer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteARecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteARecord(recordsRecordkey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'deleteARecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCNAMERecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCNAMERecord(recordsRecordkey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'deleteCNAMERecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteHostRecord(recordsRecordkey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'deleteHostRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePTRRecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePTRRecord(recordsRecordkey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Records', 'deletePTRRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let objectObjectReference = 'record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx';
    const objectType = 'record:a';
    const objectBody = {
      name: 'someZone',
      ipv4addr: '1.1.1.1'
    };
    const objectBodyUpdateed = {
      name: 'someZone',
      ipv4addr: '1.2.3.4'
    };
    const queryObject = {
      name: 'someZone',
      ipv4addr: '1.1.1.1'
    };
    const returnFields = 'name,ipv4addr';

    describe('#createObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createObject(objectType, objectBody, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              // get objectReference for update and delete tests.
              const { response: { _ref: ref = '' } = {} } = data || {};
              if (!ref) log.warn('createObject: response does not have an objectReference');
              objectObjectReference = ref || objectObjectReference;
              saveMockData('Object', 'createObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getObject(objectType, queryObject, returnFields, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'getObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateObject(objectObjectReference, objectBodyUpdateed, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'updateObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteObject(objectObjectReference, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Object', 'deleteObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restartServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const objectReference = 'grid/b25lLmNsdXN0ZXIkMA:Infoblox';
          const queries = {
            _function: 'restartservices',
            restart_option: 'RESTART_IF_NEEDED'
          };
          a.restartServices(objectReference, queries, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'restartServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensibleAttributeDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtensibleAttributeDefinition('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExtensibleAttributes', 'deleteExtensibleAttributeDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkView('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkViewsAndDNSViews', 'deleteNetworkView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRecordsByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllRecordsByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getAllRecordsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhosts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBulkhosts(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getBulkhosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateBulkhostsBodyParam = {
      comment: 'string',
      disable: 'bool',
      end_addr: 'string',
      extattrs: 'extattr',
      name_template: 'string',
      prefix: 'string',
      reverse: 'bool',
      start_addr: 'string',
      ttl: 'uint',
      use_name_template: 'bool',
      use_ttl: 'bool',
      view: 'string',
      zone: 'string'
    };
    describe('#createBulkhosts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkhosts(null, dNSCreateBulkhostsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createBulkhosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhostByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBulkhostByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getBulkhostByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateBulkhostByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      end_addr: 'string',
      extattrs: 'extattr',
      name_template: 'string',
      prefix: 'string',
      reverse: 'bool',
      start_addr: 'string',
      ttl: 'uint',
      use_name_template: 'bool',
      use_ttl: 'bool',
      view: 'string',
      zone: 'string'
    };
    describe('#updateBulkhostByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBulkhostByReference('fakedata', null, dNSUpdateBulkhostByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateBulkhostByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBulkhostByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBulkhostByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteBulkhostByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhostTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBulkhostTemplate(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getBulkhostTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateBulkhostTemplateBodyParam = {
      template_format: 'string',
      template_name: 'string'
    };
    describe('#createBulkhostTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkhostTemplate(null, dNSCreateBulkhostTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createBulkhostTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkhostTemplateByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBulkhostTemplateByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getBulkhostTemplateByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateBulkhostTemplateByReferenceBodyParam = {
      template_format: 'string',
      template_name: 'string'
    };
    describe('#updateBulkhostTemplateByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBulkhostTemplateByReference('fakedata', null, dNSUpdateBulkhostTemplateByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateBulkhostTemplateByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBulkhostTemplateByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBulkhostTemplateByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteBulkhostTemplateByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalCluster - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDdnsPrincipalCluster(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDdnsPrincipalCluster', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateDdnsPrincipalClusterBodyParam = {
      comment: 'string',
      group: 'string',
      name: 'string',
      principals: 'string'
    };
    describe('#createDdnsPrincipalCluster - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDdnsPrincipalCluster(null, dNSCreateDdnsPrincipalClusterBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createDdnsPrincipalCluster', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalClusterByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDdnsPrincipalClusterByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDdnsPrincipalClusterByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateDdnsPrincipalClusterByReferenceBodyParam = {
      comment: 'string',
      group: 'string',
      name: 'string',
      principals: 'string'
    };
    describe('#updateDdnsPrincipalClusterByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDdnsPrincipalClusterByReference('fakedata', null, dNSUpdateDdnsPrincipalClusterByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateDdnsPrincipalClusterByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDdnsPrincipalClusterByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDdnsPrincipalClusterByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDdnsPrincipalClusterByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalClusterGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDdnsPrincipalClusterGroup(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDdnsPrincipalClusterGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateDdnsPrincipalClusterGroupBodyParam = {
      comment: 'string',
      name: 'string'
    };
    describe('#createDdnsPrincipalClusterGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDdnsPrincipalClusterGroup(null, dNSCreateDdnsPrincipalClusterGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createDdnsPrincipalClusterGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDdnsPrincipalClusterGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDdnsPrincipalClusterGroupByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDdnsPrincipalClusterGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateDdnsPrincipalClusterGroupByReferenceBodyParam = {
      comment: 'string',
      name: 'string'
    };
    describe('#updateDdnsPrincipalClusterGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDdnsPrincipalClusterGroupByReference('fakedata', null, dNSUpdateDdnsPrincipalClusterGroupByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateDdnsPrincipalClusterGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDdnsPrincipalClusterGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDdnsPrincipalClusterGroupByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDdnsPrincipalClusterGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDns64Group - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDns64Group(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDns64Group', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateDns64GroupBodyParam = {
      clients: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      comment: 'string',
      disable: 'bool',
      enable_dnssec_dns64: 'bool',
      exclude: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      extattrs: 'extattr',
      mapped: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      name: 'string',
      prefix: 'string'
    };
    describe('#createDns64Group - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDns64Group(null, dNSCreateDns64GroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createDns64Group', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDns64GroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDns64GroupByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDns64GroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateDns64GroupByReferenceBodyParam = {
      clients: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      comment: 'string',
      disable: 'bool',
      enable_dnssec_dns64: 'bool',
      exclude: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      extattrs: 'extattr',
      mapped: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      name: 'string',
      prefix: 'string'
    };
    describe('#updateDns64GroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDns64GroupByReference('fakedata', null, dNSUpdateDns64GroupByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateDns64GroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDns64GroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDns64GroupByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDns64GroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridDnsByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGridDnsByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getGridDnsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateGridDnsByReferenceBodyParam = {};
    describe('#createGridDnsByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGridDnsByReference('fakedata', null, dNSCreateGridDnsByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.run_scavenging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createGridDnsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateGridDnsByReferenceBodyParam = {
      add_client_ip_mac_options: 'bool',
      allow_bulkhost_ddns: '[\'REFUSAL\', \'SUCCESS\']',
      allow_gss_tsig_zone_updates: 'bool',
      allow_query: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_recursive_query: 'bool',
      allow_transfer: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_update: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      anonymize_response_logging: 'bool',
      attack_mitigation: 'grid:attackmitigation',
      auto_blackhole: 'grid:autoblackhole',
      bind_check_names_policy: '[\'FAIL\', \'WARN\']',
      bind_hostname_directive: '[\'NONE\', \'HOSTNAME\']',
      blackhole_list: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      blacklist_action: '[\'REDIRECT\', \'REFUSE\']',
      blacklist_log_query: 'bool',
      blacklist_redirect_addresses: 'string',
      blacklist_redirect_ttl: 'uint',
      blacklist_rulesets: 'string',
      bulk_host_name_templates: 'bulkhostnametemplate',
      capture_dns_queries_on_all_domains: 'bool',
      check_names_for_ddns_and_zone_transfer: 'bool',
      client_subnet_domains: '[{\'domain\': \'string\', \'permission\': \'enum\'}]',
      client_subnet_ipv4_prefix_length: 'uint',
      client_subnet_ipv6_prefix_length: 'uint',
      copy_client_ip_mac_options: 'bool',
      copy_xfer_to_notify: 'bool',
      custom_root_name_servers: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      ddns_force_creation_timestamp_update: 'bool',
      ddns_principal_group: 'string',
      ddns_principal_tracking: 'bool',
      ddns_restrict_patterns: 'bool',
      ddns_restrict_patterns_list: 'string',
      ddns_restrict_protected: 'bool',
      ddns_restrict_secure: 'bool',
      ddns_restrict_static: 'bool',
      default_bulk_host_name_template: 'string',
      default_ttl: 'uint',
      disable_edns: 'bool',
      dns64_groups: 'string',
      dns_cache_acceleration_ttl: 'uint',
      dns_health_check_anycast_control: 'bool',
      dns_health_check_domain_list: 'string',
      dns_health_check_interval: 'uint',
      dns_health_check_recursion_flag: 'bool',
      dns_health_check_retries: 'uint',
      dns_health_check_timeout: 'uint',
      dns_query_capture_file_time_limit: 'uint',
      dnssec_blacklist_enabled: 'bool',
      dnssec_dns64_enabled: 'bool',
      dnssec_enabled: 'bool',
      dnssec_expired_signatures_enabled: 'bool',
      dnssec_key_params: 'dnsseckeyparams',
      dnssec_negative_trust_anchors: 'string',
      dnssec_nxdomain_enabled: 'bool',
      dnssec_rpz_enabled: 'bool',
      dnssec_trusted_keys: '[{\'fqdn\': \'string\', \'algorithm\': \'string\', \'key\': \'string\', \'secure_entry_point\': \'bool\', \'dnssec_must_be_secure\': \'bool\'}]',
      dnssec_validation_enabled: 'bool',
      dnstap_setting: 'dnstapsetting',
      domains_to_capture_dns_queries: 'string',
      dtc_dnssec_mode: '[\'SIGNED\', \'UNSIGNED\']',
      dtc_edns_prefer_client_subnet: 'bool',
      dtc_scheduled_backup: 'scheduledbackup',
      dtc_topology_ea_list: 'string',
      email: 'string',
      enable_blackhole: 'bool',
      enable_blacklist: 'bool',
      enable_capture_dns_queries: 'bool',
      enable_capture_dns_responses: 'bool',
      enable_client_subnet_forwarding: 'bool',
      enable_client_subnet_recursive: 'bool',
      enable_delete_associated_ptr: 'bool',
      enable_dns64: 'bool',
      enable_dns_health_check: 'bool',
      enable_dnstap_queries: 'bool',
      enable_dnstap_responses: 'bool',
      enable_dtc_dns_fall_through: 'bool',
      enable_excluded_domain_names: 'bool',
      enable_fixed_rrset_order_fqdns: 'bool',
      enable_ftc: 'bool',
      enable_gss_tsig: 'bool',
      enable_host_rrset_order: 'bool',
      enable_hsm_signing: 'bool',
      enable_notify_source_port: 'bool',
      enable_query_rewrite: 'bool',
      enable_query_source_port: 'bool',
      excluded_domain_names: 'string',
      expire_after: 'uint',
      file_transfer_setting: 'filetransfersetting',
      filter_aaaa: '[\'YES\', \'NO\', \'BREAK_DNSSEC\']',
      filter_aaaa_list: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      fixed_rrset_order_fqdns: '[{\'fqdn\': \'string\', \'record_type\': \'enum\'}]',
      forward_only: 'bool',
      forward_updates: 'bool',
      forwarders: 'string',
      ftc_expired_record_timeout: 'uint',
      ftc_expired_record_ttl: 'uint',
      gss_tsig_keys: 'kerberoskey',
      lame_ttl: 'uint',
      logging_categories: 'grid:loggingcategories',
      max_cache_ttl: 'uint',
      max_cached_lifetime: 'uint',
      max_ncache_ttl: 'uint',
      member_secondary_notify: 'bool',
      negative_ttl: 'uint',
      notify_delay: 'uint',
      notify_source_port: 'uint',
      nsgroup_default: 'string',
      nsgroups: 'string',
      nxdomain_log_query: 'bool',
      nxdomain_redirect: 'bool',
      nxdomain_redirect_addresses: 'string',
      nxdomain_redirect_addresses_v6: 'string',
      nxdomain_redirect_ttl: 'uint',
      nxdomain_rulesets: 'string',
      preserve_host_rrset_order_on_secondaries: 'bool',
      protocol_record_name_policies: 'recordnamepolicy',
      query_rewrite_domain_names: 'string',
      query_rewrite_prefix: 'string',
      query_source_port: 'uint',
      recursive_query_list: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      refresh_timer: 'uint',
      resolver_query_timeout: 'uint',
      response_rate_limiting: 'grid:responseratelimiting',
      restart_setting: 'grid:servicerestart',
      retry_timer: 'uint',
      root_name_server_type: '[\'CUSTOM\', \'INTERNET\']',
      rpz_disable_nsdname_nsip: 'bool',
      rpz_drop_ip_rule_enabled: 'bool',
      rpz_drop_ip_rule_min_prefix_length_ipv4: 'uint',
      rpz_drop_ip_rule_min_prefix_length_ipv6: 'uint',
      rpz_qname_wait_recurse: 'bool',
      scavenging_settings: 'setting:scavenging',
      serial_query_rate: 'uint',
      server_id_directive: '[\'NONE\', \'HOSTNAME\']',
      sortlist: '[{\'address\': \'string\', \'match_list\': \'string\'}]',
      store_locally: 'bool',
      syslog_facility: '[\'DAEMON\', \'LOCAL0\', \'LOCAL1\', \'LOCAL2\', \'LOCAL3\', \'LOCAL4\', \'LOCAL5\', \'LOCAL6\', \'LOCAL7\']',
      transfer_excluded_servers: 'string',
      transfer_format: '[\'MANY_ANSWERS\', \'ONE_ANSWER\']',
      transfers_in: 'uint',
      transfers_out: 'uint',
      transfers_per_ns: 'uint',
      zone_deletion_double_confirm: 'bool'
    };
    describe('#updateGridDnsByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGridDnsByReference('fakedata', null, dNSUpdateGridDnsByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateGridDnsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostnameRewritePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostnameRewritePolicy(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getHostnameRewritePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostnameRewritePolicyByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostnameRewritePolicyByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getHostnameRewritePolicyByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDnsByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMemberDnsByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getMemberDnsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateMemberDnsByReferenceBodyParam = {};
    describe('#createMemberDnsByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMemberDnsByReference('fakedata', null, dNSCreateMemberDnsByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.clear_dns_cache);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createMemberDnsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateMemberDnsByReferenceBodyParam = {
      add_client_ip_mac_options: 'bool',
      additional_ip_list: 'string',
      additional_ip_list_struct: '[{\'ip_address\': \'string\', \'ipsd\': \'string\'}]',
      allow_gss_tsig_zone_updates: 'bool',
      allow_query: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_recursive_query: 'bool',
      allow_transfer: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_update: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      anonymize_response_logging: 'bool',
      atc_fwd_enable: 'bool',
      atc_fwd_forward_first: 'bool',
      attack_mitigation: 'grid:attackmitigation',
      auto_blackhole: 'grid:autoblackhole',
      auto_create_a_and_ptr_for_lan2: 'bool',
      auto_create_aaaa_and_ipv6ptr_for_lan2: 'bool',
      auto_sort_views: 'bool',
      bind_check_names_policy: '[\'FAIL\', \'WARN\']',
      bind_hostname_directive: '[\'NONE\', \'HOSTNAME\', \'USER_DEFINED\']',
      bind_hostname_directive_fqdn: 'string',
      blackhole_list: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      blacklist_action: '[\'REDIRECT\', \'REFUSE\']',
      blacklist_log_query: 'bool',
      blacklist_redirect_addresses: 'string',
      blacklist_redirect_ttl: 'uint',
      blacklist_rulesets: 'string',
      capture_dns_queries_on_all_domains: 'bool',
      check_names_for_ddns_and_zone_transfer: 'bool',
      copy_client_ip_mac_options: 'bool',
      copy_xfer_to_notify: 'bool',
      custom_root_name_servers: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      disable_edns: 'bool',
      dns64_groups: 'string',
      dns_cache_acceleration_ttl: 'uint',
      dns_health_check_anycast_control: 'bool',
      dns_health_check_domain_list: 'string',
      dns_health_check_interval: 'uint',
      dns_health_check_recursion_flag: 'bool',
      dns_health_check_retries: 'uint',
      dns_health_check_timeout: 'uint',
      dns_notify_transfer_source: '[\'VIP\', \'MGMT\', \'LAN2\', \'ANY\', \'IP\']',
      dns_notify_transfer_source_address: 'string',
      dns_query_capture_file_time_limit: 'uint',
      dns_query_source_address: 'string',
      dns_query_source_interface: '[\'VIP\', \'MGMT\', \'LAN2\', \'ANY\', \'IP\']',
      dns_view_address_settings: '[{\'view_name\': \'string\', \'dns_notify_transfer_source\': \'enum\', \'dns_notify_transfer_source_address\': \'string\', \'dns_query_source_interface\': \'enum\', \'dns_query_source_address\': \'string\', \'enable_notify_source_port\': \'bool\', \'notify_source_port\': \'uint\', \'enable_query_source_port\': \'bool\', \'query_source_port\': \'uint\', \'notify_delay\': \'uint\', \'use_source_ports\': \'bool\', \'use_notify_delay\': \'bool\'}]',
      dnssec_blacklist_enabled: 'bool',
      dnssec_dns64_enabled: 'bool',
      dnssec_enabled: 'bool',
      dnssec_expired_signatures_enabled: 'bool',
      dnssec_negative_trust_anchors: 'string',
      dnssec_nxdomain_enabled: 'bool',
      dnssec_rpz_enabled: 'bool',
      dnssec_trusted_keys: '[{\'fqdn\': \'string\', \'algorithm\': \'string\', \'key\': \'string\', \'secure_entry_point\': \'bool\', \'dnssec_must_be_secure\': \'bool\'}]',
      dnssec_validation_enabled: 'bool',
      dnstap_setting: 'dnstapsetting',
      domains_to_capture_dns_queries: 'string',
      dtc_edns_prefer_client_subnet: 'bool',
      dtc_health_source: '[\'VIP\', \'MGMT\', \'LAN2\', \'ANY\', \'IP\']',
      dtc_health_source_address: 'string',
      enable_blackhole: 'bool',
      enable_blacklist: 'bool',
      enable_capture_dns_queries: 'bool',
      enable_capture_dns_responses: 'bool',
      enable_dns: 'bool',
      enable_dns64: 'bool',
      enable_dns_cache_acceleration: 'bool',
      enable_dns_health_check: 'bool',
      enable_dnstap_queries: 'bool',
      enable_dnstap_responses: 'bool',
      enable_excluded_domain_names: 'bool',
      enable_fixed_rrset_order_fqdns: 'bool',
      enable_ftc: 'bool',
      enable_gss_tsig: 'bool',
      enable_notify_source_port: 'bool',
      enable_query_rewrite: 'bool',
      enable_query_source_port: 'bool',
      excluded_domain_names: 'string',
      extattrs: 'extattr',
      file_transfer_setting: 'filetransfersetting',
      filter_aaaa: '[\'YES\', \'NO\', \'BREAK_DNSSEC\']',
      filter_aaaa_list: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      fixed_rrset_order_fqdns: '[{\'fqdn\': \'string\', \'record_type\': \'enum\'}]',
      forward_only: 'bool',
      forward_updates: 'bool',
      forwarders: 'string',
      ftc_expired_record_timeout: 'uint',
      ftc_expired_record_ttl: 'uint',
      glue_record_addresses: '[{\'attach_empty_recursive_view\': \'bool\', \'glue_record_address\': \'string\', \'view\': \'string\', \'glue_address_choice\': \'enum\'}]',
      gss_tsig_keys: 'kerberoskey',
      ipv6_glue_record_addresses: '[{\'attach_empty_recursive_view\': \'bool\', \'glue_record_address\': \'string\', \'view\': \'string\', \'glue_address_choice\': \'enum\'}]',
      lame_ttl: 'uint',
      lan1_ipsd: 'string',
      lan1_ipv6_ipsd: 'string',
      lan2_ipsd: 'string',
      lan2_ipv6_ipsd: 'string',
      logging_categories: 'grid:loggingcategories',
      max_cache_ttl: 'uint',
      max_cached_lifetime: 'uint',
      max_ncache_ttl: 'uint',
      mgmt_ipsd: 'string',
      mgmt_ipv6_ipsd: 'string',
      minimal_resp: 'bool',
      notify_delay: 'uint',
      notify_source_port: 'uint',
      nxdomain_log_query: 'bool',
      nxdomain_redirect: 'bool',
      nxdomain_redirect_addresses: 'string',
      nxdomain_redirect_addresses_v6: 'string',
      nxdomain_redirect_ttl: 'uint',
      nxdomain_rulesets: 'string',
      query_source_port: 'uint',
      record_name_policy: 'string',
      recursive_client_limit: 'uint',
      recursive_query_list: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      recursive_resolver: '[\'BIND\', \'UNBOUND\']',
      resolver_query_timeout: 'uint',
      response_rate_limiting: 'grid:responseratelimiting',
      root_name_server_type: '[\'CUSTOM\', \'INTERNET\']',
      rpz_disable_nsdname_nsip: 'bool',
      rpz_drop_ip_rule_enabled: 'bool',
      rpz_drop_ip_rule_min_prefix_length_ipv4: 'uint',
      rpz_drop_ip_rule_min_prefix_length_ipv6: 'uint',
      rpz_qname_wait_recurse: 'bool',
      serial_query_rate: 'uint',
      server_id_directive: '[\'NONE\', \'HOSTNAME\', \'USER_DEFINED\']',
      server_id_directive_string: 'string',
      skip_in_grid_rpz_queries: 'bool',
      sortlist: '[{\'address\': \'string\', \'match_list\': \'string\'}]',
      store_locally: 'bool',
      syslog_facility: '[\'DAEMON\', \'LOCAL0\', \'LOCAL1\', \'LOCAL2\', \'LOCAL3\', \'LOCAL4\', \'LOCAL5\', \'LOCAL6\', \'LOCAL7\']',
      transfer_excluded_servers: 'string',
      transfer_format: '[\'MANY_ANSWERS\', \'ONE_ANSWER\']',
      transfers_in: 'uint',
      transfers_out: 'uint',
      transfers_per_ns: 'uint',
      unbound_logging_level: '[\'ERRORS_ONLY\', \'OPERATIONS\', \'DETAILED_OPERATIONS\', \'QUERY\', \'ALGORITHM\', \'CACHE_MISSES\']',
      use_add_client_ip_mac_options: 'bool',
      use_allow_query: 'bool',
      use_allow_transfer: 'bool',
      use_attack_mitigation: 'bool',
      use_auto_blackhole: 'bool',
      use_bind_hostname_directive: 'bool',
      use_blackhole: 'bool',
      use_blacklist: 'bool',
      use_capture_dns_queries_on_all_domains: 'bool',
      use_copy_client_ip_mac_options: 'bool',
      use_copy_xfer_to_notify: 'bool',
      use_disable_edns: 'bool',
      use_dns64: 'bool',
      use_dns_cache_acceleration_ttl: 'bool',
      use_dns_health_check: 'bool',
      use_dnssec: 'bool',
      use_dnstap_setting: 'bool',
      use_dtc_edns_prefer_client_subnet: 'bool',
      use_enable_capture_dns: 'bool',
      use_enable_excluded_domain_names: 'bool',
      use_enable_gss_tsig: 'bool',
      use_enable_query_rewrite: 'bool',
      use_filter_aaaa: 'bool',
      use_fixed_rrset_order_fqdns: 'bool',
      use_forward_updates: 'bool',
      use_forwarders: 'bool',
      use_ftc: 'bool',
      use_gss_tsig_keys: 'bool',
      use_lame_ttl: 'bool',
      use_lan2_ipv6_port: 'bool',
      use_lan2_port: 'bool',
      use_lan_ipv6_port: 'bool',
      use_lan_port: 'bool',
      use_logging_categories: 'bool',
      use_max_cache_ttl: 'bool',
      use_max_cached_lifetime: 'bool',
      use_max_ncache_ttl: 'bool',
      use_mgmt_ipv6_port: 'bool',
      use_mgmt_port: 'bool',
      use_notify_delay: 'bool',
      use_nxdomain_redirect: 'bool',
      use_record_name_policy: 'bool',
      use_recursive_client_limit: 'bool',
      use_recursive_query_setting: 'bool',
      use_resolver_query_timeout: 'bool',
      use_response_rate_limiting: 'bool',
      use_root_name_server: 'bool',
      use_rpz_disable_nsdname_nsip: 'bool',
      use_rpz_drop_ip_rule: 'bool',
      use_rpz_qname_wait_recurse: 'bool',
      use_serial_query_rate: 'bool',
      use_server_id_directive: 'bool',
      use_sortlist: 'bool',
      use_source_ports: 'bool',
      use_syslog_facility: 'bool',
      use_transfers_in: 'bool',
      use_transfers_out: 'bool',
      use_transfers_per_ns: 'bool',
      use_update_setting: 'bool',
      use_zone_transfer_format: 'bool',
      views: 'string'
    };
    describe('#updateMemberDnsByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMemberDnsByReference('fakedata', null, dNSUpdateMemberDnsByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateMemberDnsByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNsGroupByReferenceBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      external_primaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      external_secondaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      grid_primary: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      grid_secondaries: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      is_grid_default: 'bool',
      name: 'string',
      use_external_primary: 'bool'
    };
    describe('#updateNsGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNsGroupByReference('fakedata', null, dNSUpdateNsGroupByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNsGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsGroupByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNsGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupDelegation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupDelegation(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupDelegation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateNsGroupDelegationBodyParam = {
      comment: 'string',
      delegate_to: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      extattrs: 'extattr',
      name: 'string'
    };
    describe('#createNsGroupDelegation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNsGroupDelegation(null, dNSCreateNsGroupDelegationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createNsGroupDelegation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupDelegationByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupDelegationByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupDelegationByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNsGroupDelegationByReferenceBodyParam = {
      comment: 'string',
      delegate_to: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      extattrs: 'extattr',
      name: 'string'
    };
    describe('#updateNsGroupDelegationByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNsGroupDelegationByReference('fakedata', null, dNSUpdateNsGroupDelegationByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNsGroupDelegationByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupDelegationByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsGroupDelegationByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNsGroupDelegationByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupForwardMember(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupForwardMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateNsGroupForwardMemberBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      forwarding_servers: '[{\'name\': \'string\', \'forwarders_only\': \'bool\', \'forward_to\': \'extserver\', \'use_override_forwarders\': \'bool\'}]',
      name: 'string'
    };
    describe('#createNsGroupForwardMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNsGroupForwardMember(null, dNSCreateNsGroupForwardMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createNsGroupForwardMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardMemberByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupForwardMemberByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupForwardMemberByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNsGroupForwardMemberByReferenceBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      forwarding_servers: '[{\'name\': \'string\', \'forwarders_only\': \'bool\', \'forward_to\': \'extserver\', \'use_override_forwarders\': \'bool\'}]',
      name: 'string'
    };
    describe('#updateNsGroupForwardMemberByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNsGroupForwardMemberByReference('fakedata', null, dNSUpdateNsGroupForwardMemberByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNsGroupForwardMemberByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupForwardMemberByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsGroupForwardMemberByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNsGroupForwardMemberByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardStubServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupForwardStubServer(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupForwardStubServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateNsGroupForwardStubServerBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      external_servers: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      name: 'string'
    };
    describe('#createNsGroupForwardStubServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNsGroupForwardStubServer(null, dNSCreateNsGroupForwardStubServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createNsGroupForwardStubServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupForwardStubServerByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupForwardStubServerByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupForwardStubServerByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNsGroupForwardStubServerByReferenceBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      external_servers: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      name: 'string'
    };
    describe('#updateNsGroupForwardStubServerByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNsGroupForwardStubServerByReference('fakedata', null, dNSUpdateNsGroupForwardStubServerByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNsGroupForwardStubServerByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupForwardStubServerByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsGroupForwardStubServerByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNsGroupForwardStubServerByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupStubmember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupStubmember(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupStubmember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateNsGroupStubmemberBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      name: 'string',
      stub_members: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]'
    };
    describe('#createNsGroupStubmember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNsGroupStubmember(null, dNSCreateNsGroupStubmemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createNsGroupStubmember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsGroupStubmemberByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsGroupStubmemberByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsGroupStubmemberByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNsGroupStubmemberByReferenceBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      name: 'string',
      stub_members: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]'
    };
    describe('#updateNsGroupStubmemberByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNsGroupStubmemberByReference('fakedata', null, dNSUpdateNsGroupStubmemberByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNsGroupStubmemberByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsGroupStubmemberByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsGroupStubmemberByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNsGroupStubmemberByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getArecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAAAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAAAArecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getAAAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateAAAArecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      ipv6addr: 'string',
      name: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateAAAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAAAArecordByReference('fakedata', null, dNSUpdateAAAArecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateAAAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAAAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAAAArecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteAAAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAliasrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAliasrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getAliasrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateAliasrecordBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\']',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      target_name: 'string',
      target_type: '[\'A\', \'AAAA\', \'MX\', \'NAPTR\', \'PTR\', \'SPF\', \'SRV\', \'TXT\']',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#createAliasrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAliasrecord(null, dNSCreateAliasrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createAliasrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAliasrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAliasrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getAliasrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateAliasrecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\']',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      target_name: 'string',
      target_type: '[\'A\', \'AAAA\', \'MX\', \'NAPTR\', \'PTR\', \'SPF\', \'SRV\', \'TXT\']',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#updateAliasrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAliasrecordByReference('fakedata', null, dNSUpdateAliasrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateAliasrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAliasrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAliasrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteAliasrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCAArecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCAArecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getCAArecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateCAArecordBodyParam = {
      ca_flag: 'uint',
      ca_tag: 'string',
      ca_value: 'string',
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      name: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#createCAArecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCAArecord(null, dNSCreateCAArecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createCAArecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCAArecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getCAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateCAArecordByReferenceBodyParam = {
      ca_flag: 'uint',
      ca_tag: 'string',
      ca_value: 'string',
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      name: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#updateCAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCAArecordByReference('fakedata', null, dNSUpdateCAArecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateCAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCAArecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteCAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCnamerecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getCnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcidrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDhcidrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDhcidrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDhcidrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDhcidrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDhcidrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcidrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDhcidrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDhcidrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnamerecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnamerecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDnamerecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateDnamerecordBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      name: 'string',
      target: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#createDnamerecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDnamerecord(null, dNSCreateDnamerecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createDnamerecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnamerecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateDnamerecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      name: 'string',
      target: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateDnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDnamerecordByReference('fakedata', null, dNSUpdateDnamerecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateDnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDnamerecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnskeyrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnskeyrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDnskeyrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnskeyrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDnskeyrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDnskeyrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDsrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDsrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDsrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDsrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDsrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDsrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDsrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDsrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteDsrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtclbdnrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDtclbdnrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDtclbdnrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtclbdnrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDtclbdnrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getDtclbdnrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv4Addrrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostIpv4Addrrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getHostIpv4Addrrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv4AddrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostIpv4AddrrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getHostIpv4AddrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateHostIpv4AddrrecordByReferenceBodyParam = {
      bootfile: 'string',
      bootserver: 'string',
      configure_for_dhcp: 'bool',
      deny_bootp: 'bool',
      enable_pxe_lease_time: 'bool',
      ignore_client_requested_options: 'bool',
      ipv4addr: 'string',
      logic_filter_rules: '[{\'filter\': \'string\', \'type\': \'string\'}]',
      mac: 'string',
      match_client: 'string',
      nextserver: 'string',
      options: '[{\'name\': \'string\', \'num\': \'uint\', \'vendor_class\': \'string\', \'value\': \'string\', \'use_option\': \'bool\'}]',
      pxe_lease_time: 'uint',
      reserved_interface: 'string',
      use_bootfile: 'bool',
      use_bootserver: 'bool',
      use_deny_bootp: 'bool',
      use_for_ea_inheritance: 'bool',
      use_ignore_client_requested_options: 'bool',
      use_logic_filter_rules: 'bool',
      use_nextserver: 'bool',
      use_options: 'bool',
      use_pxe_lease_time: 'bool'
    };
    describe('#updateHostIpv4AddrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateHostIpv4AddrrecordByReference('fakedata', null, dNSUpdateHostIpv4AddrrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateHostIpv4AddrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv6Addrrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostIpv6Addrrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getHostIpv6Addrrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostIpv6AddrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getHostIpv6AddrrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getHostIpv6AddrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateHostIpv6AddrrecordByReferenceBodyParam = {
      address_type: '[\'ADDRESS\', \'PREFIX\', \'BOTH\']',
      configure_for_dhcp: 'bool',
      domain_name: 'string',
      domain_name_servers: 'string',
      duid: 'string',
      ipv6addr: 'string',
      ipv6prefix: 'string',
      ipv6prefix_bits: 'uint',
      match_client: 'string',
      options: '[{\'name\': \'string\', \'num\': \'uint\', \'vendor_class\': \'string\', \'value\': \'string\', \'use_option\': \'bool\'}]',
      preferred_lifetime: 'uint',
      reserved_interface: 'string',
      use_domain_name: 'bool',
      use_domain_name_servers: 'bool',
      use_for_ea_inheritance: 'bool',
      use_options: 'bool',
      use_preferred_lifetime: 'bool',
      use_valid_lifetime: 'bool',
      valid_lifetime: 'uint'
    };
    describe('#updateHostIpv6AddrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateHostIpv6AddrrecordByReference('fakedata', null, dNSUpdateHostIpv6AddrrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateHostIpv6AddrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMxrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMxrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getMxrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateMxrecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      mail_exchanger: 'string',
      name: 'string',
      preference: 'uint',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#updateMxrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMxrecordByReference('fakedata', null, dNSUpdateMxrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateMxrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMxrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMxrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteMxrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNaptrrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNaptrrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNaptrrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateNaptrrecordBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      flags: 'string',
      forbid_reclamation: 'bool',
      name: 'string',
      order: 'uint',
      preference: 'uint',
      regexp: 'string',
      replacement: 'string',
      services: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#createNaptrrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNaptrrecord(null, dNSCreateNaptrrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createNaptrrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNaptrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNaptrrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNaptrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNaptrrecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      flags: 'string',
      forbid_reclamation: 'bool',
      name: 'string',
      order: 'uint',
      preference: 'uint',
      regexp: 'string',
      replacement: 'string',
      services: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateNaptrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNaptrrecordByReference('fakedata', null, dNSUpdateNaptrrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNaptrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNaptrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNaptrrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNaptrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateNsrecordByReferenceBodyParam = {
      addresses: '[{\'address\': \'string\', \'auto_create_ptr\': \'bool\'}]',
      ms_delegation_name: 'string',
      nameserver: 'string'
    };
    describe('#updateNsrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNsrecordByReference('fakedata', null, dNSUpdateNsrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateNsrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNsrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNsrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteNsrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsecrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsecrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsecrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsecrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsecrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsecrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3record - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsec3record(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsec3record', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3recordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsec3recordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsec3recordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3paramrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsec3paramrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsec3paramrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsec3paramrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNsec3paramrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getNsec3paramrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPtrrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPtrrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getPtrrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRrsigrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRrsigrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getRrsigrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRrsigrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRrsigrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getRrsigrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrvrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSrvrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSrvrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSrvrecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      name: 'string',
      port: 'uint',
      priority: 'uint',
      target: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      weight: 'uint'
    };
    describe('#updateSrvrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSrvrecordByReference('fakedata', null, dNSUpdateSrvrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSrvrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSrvrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSrvrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSrvrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTlsarecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTlsarecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getTlsarecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateTlsarecordBodyParam = {
      certificate_data: 'string',
      certificate_usage: 'uint',
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      disable: 'bool',
      extattrs: 'extattr',
      matched_type: 'uint',
      name: 'string',
      selector: 'uint',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#createTlsarecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTlsarecord(null, dNSCreateTlsarecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createTlsarecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTlsarecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTlsarecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getTlsarecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateTlsarecordByReferenceBodyParam = {
      certificate_data: 'string',
      certificate_usage: 'uint',
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      disable: 'bool',
      extattrs: 'extattr',
      matched_type: 'uint',
      name: 'string',
      selector: 'uint',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#updateTlsarecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTlsarecordByReference('fakedata', null, dNSUpdateTlsarecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateTlsarecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTlsarecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTlsarecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteTlsarecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTxtrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTxtrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getTxtrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateTxtrecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      ddns_principal: 'string',
      ddns_protected: 'bool',
      disable: 'bool',
      extattrs: 'extattr',
      forbid_reclamation: 'bool',
      name: 'string',
      text: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#updateTxtrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTxtrecordByReference('fakedata', null, dNSUpdateTxtrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateTxtrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTxtrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTxtrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteTxtrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUnknownrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getUnknownrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateUnknownrecordBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      disable: 'bool',
      enable_host_name_policy: 'bool',
      extattrs: 'extattr',
      name: 'string',
      record_type: 'string',
      subfield_values: '[{\'field_value\': \'string\', \'field_type\': \'string\', \'include_length\': \'enum\'}]',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#createUnknownrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUnknownrecord(null, dNSCreateUnknownrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createUnknownrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUnknownrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getUnknownrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateUnknownrecordByReferenceBodyParam = {
      comment: 'string',
      creator: '[\'STATIC\', \'DYNAMIC\', \'SYSTEM\']',
      disable: 'bool',
      enable_host_name_policy: 'bool',
      extattrs: 'extattr',
      name: 'string',
      record_type: 'string',
      subfield_values: '[{\'field_value\': \'string\', \'field_type\': \'string\', \'include_length\': \'enum\'}]',
      ttl: 'uint',
      use_ttl: 'bool',
      view: 'string'
    };
    describe('#updateUnknownrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUnknownrecordByReference('fakedata', null, dNSUpdateUnknownrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateUnknownrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnknownrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUnknownrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteUnknownrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordNamePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRecordNamePolicy(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getRecordNamePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateRecordNamePolicyBodyParam = {
      is_default: 'bool',
      name: 'string',
      regex: 'string'
    };
    describe('#createRecordNamePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRecordNamePolicy(null, dNSCreateRecordNamePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createRecordNamePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecordNamePolicyByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRecordNamePolicyByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getRecordNamePolicyByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateRecordNamePolicyByReferenceBodyParam = {
      is_default: 'bool',
      name: 'string',
      regex: 'string'
    };
    describe('#updateRecordNamePolicyByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRecordNamePolicyByReference('fakedata', null, dNSUpdateRecordNamePolicyByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateRecordNamePolicyByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRecordNamePolicyByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRecordNamePolicyByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteRecordNamePolicyByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRuleset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRuleset(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getRuleset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateRulesetBodyParam = {
      comment: 'string',
      disabled: 'bool',
      name: 'string',
      nxdomain_rules: '[{\'action\': \'enum\', \'pattern\': \'string\'}]',
      type: '[\'NXDOMAIN\', \'BLACKLIST\']'
    };
    describe('#createRuleset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRuleset(null, dNSCreateRulesetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createRuleset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRulesetByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRulesetByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getRulesetByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateRulesetByReferenceBodyParam = {
      comment: 'string',
      disabled: 'bool',
      name: 'string',
      nxdomain_rules: '[{\'action\': \'enum\', \'pattern\': \'string\'}]',
      type: '[\'NXDOMAIN\', \'BLACKLIST\']'
    };
    describe('#updateRulesetByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRulesetByReference('fakedata', null, dNSUpdateRulesetByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateRulesetByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRulesetByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRulesetByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteRulesetByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScavengingTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getScavengingTask(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getScavengingTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScavengingTaskByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getScavengingTaskByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getScavengingTaskByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedArecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedArecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedArecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedArecordBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      ipv4addr: 'string',
      name: 'string',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#createSharedArecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedArecord(null, dNSCreateSharedArecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedArecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedArecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedArecordByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      ipv4addr: 'string',
      name: 'string',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateSharedArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedArecordByReference('fakedata', null, dNSUpdateSharedArecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedArecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedAAAArecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedAAAArecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedAAAArecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedAAAArecordBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      ipv6addr: 'string',
      name: 'string',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#createSharedAAAArecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedAAAArecord(null, dNSCreateSharedAAAArecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedAAAArecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedAAAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedAAAArecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedAAAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedAAAArecordByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      ipv6addr: 'string',
      name: 'string',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateSharedAAAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedAAAArecordByReference('fakedata', null, dNSUpdateSharedAAAArecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedAAAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedAAAArecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedAAAArecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedAAAArecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedCnamerecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedCnamerecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedCnamerecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedCnamerecordBodyParam = {
      canonical: 'string',
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#createSharedCnamerecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedCnamerecord(null, dNSCreateSharedCnamerecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedCnamerecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedCnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedCnamerecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedCnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedCnamerecordByReferenceBodyParam = {
      canonical: 'string',
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateSharedCnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedCnamerecordByReference('fakedata', null, dNSUpdateSharedCnamerecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedCnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedCnamerecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedCnamerecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedCnamerecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedMxrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedMxrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedMxrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedMxrecordBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      mail_exchanger: 'string',
      name: 'string',
      preference: 'uint',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#createSharedMxrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedMxrecord(null, dNSCreateSharedMxrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedMxrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedMxrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedMxrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedMxrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedMxrecordByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      mail_exchanger: 'string',
      name: 'string',
      preference: 'uint',
      shared_record_group: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateSharedMxrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedMxrecordByReference('fakedata', null, dNSUpdateSharedMxrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedMxrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedMxrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedMxrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedMxrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedSrvrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedSrvrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedSrvrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedSrvrecordBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      port: 'uint',
      priority: 'uint',
      shared_record_group: 'string',
      target: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      weight: 'uint'
    };
    describe('#createSharedSrvrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedSrvrecord(null, dNSCreateSharedSrvrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedSrvrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedSrvrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedSrvrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedSrvrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedSrvrecordByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      port: 'uint',
      priority: 'uint',
      shared_record_group: 'string',
      target: 'string',
      ttl: 'uint',
      use_ttl: 'bool',
      weight: 'uint'
    };
    describe('#updateSharedSrvrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedSrvrecordByReference('fakedata', null, dNSUpdateSharedSrvrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedSrvrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedSrvrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedSrvrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedSrvrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedTxtrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedTxtrecord(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedTxtrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedTxtrecordBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      shared_record_group: 'string',
      text: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#createSharedTxtrecord - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedTxtrecord(null, dNSCreateSharedTxtrecordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedTxtrecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedTxtrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedTxtrecordByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedTxtrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedTxtrecordByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      name: 'string',
      shared_record_group: 'string',
      text: 'string',
      ttl: 'uint',
      use_ttl: 'bool'
    };
    describe('#updateSharedTxtrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedTxtrecordByReference('fakedata', null, dNSUpdateSharedTxtrecordByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedTxtrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedTxtrecordByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedTxtrecordByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedTxtrecordByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedRecordGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedRecordGroup(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedRecordGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateSharedRecordGroupBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      name: 'string',
      record_name_policy: 'string',
      use_record_name_policy: 'bool',
      zone_associations: 'string'
    };
    describe('#createSharedRecordGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSharedRecordGroup(null, dNSCreateSharedRecordGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createSharedRecordGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSharedRecordGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSharedRecordGroupByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getSharedRecordGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateSharedRecordGroupByReferenceBodyParam = {
      comment: 'string',
      extattrs: 'extattr',
      name: 'string',
      record_name_policy: 'string',
      use_record_name_policy: 'bool',
      zone_associations: 'string'
    };
    describe('#updateSharedRecordGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSharedRecordGroupByReference('fakedata', null, dNSUpdateSharedRecordGroupByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateSharedRecordGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSharedRecordGroupByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSharedRecordGroupByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteSharedRecordGroupByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateViewBodyParam = {
      blacklist_action: '[\'REDIRECT\', \'REFUSE\']',
      blacklist_log_query: 'bool',
      blacklist_redirect_addresses: 'string',
      blacklist_redirect_ttl: 'uint',
      blacklist_rulesets: 'string',
      comment: 'string',
      custom_root_name_servers: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      ddns_force_creation_timestamp_update: 'bool',
      ddns_principal_group: 'string',
      ddns_principal_tracking: 'bool',
      ddns_restrict_patterns: 'bool',
      ddns_restrict_patterns_list: 'string',
      ddns_restrict_protected: 'bool',
      ddns_restrict_secure: 'bool',
      ddns_restrict_static: 'bool',
      disable: 'bool',
      dns64_enabled: 'bool',
      dns64_groups: 'string',
      dnssec_enabled: 'bool',
      dnssec_expired_signatures_enabled: 'bool',
      dnssec_negative_trust_anchors: 'string',
      dnssec_trusted_keys: '[{\'fqdn\': \'string\', \'algorithm\': \'string\', \'key\': \'string\', \'secure_entry_point\': \'bool\', \'dnssec_must_be_secure\': \'bool\'}]',
      dnssec_validation_enabled: 'bool',
      enable_blacklist: 'bool',
      enable_fixed_rrset_order_fqdns: 'bool',
      enable_match_recursive_only: 'bool',
      extattrs: 'extattr',
      filter_aaaa: '[\'YES\', \'NO\', \'BREAK_DNSSEC\']',
      filter_aaaa_list: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      fixed_rrset_order_fqdns: '[{\'fqdn\': \'string\', \'record_type\': \'enum\'}]',
      forward_only: 'bool',
      forwarders: 'string',
      lame_ttl: 'uint',
      match_clients: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      match_destinations: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      max_cache_ttl: 'uint',
      max_ncache_ttl: 'uint',
      name: 'string',
      network_view: 'string',
      notify_delay: 'uint',
      nxdomain_log_query: 'bool',
      nxdomain_redirect: 'bool',
      nxdomain_redirect_addresses: 'string',
      nxdomain_redirect_addresses_v6: 'string',
      nxdomain_redirect_ttl: 'uint',
      nxdomain_rulesets: 'string',
      recursion: 'bool',
      response_rate_limiting: 'grid:responseratelimiting',
      root_name_server_type: '[\'CUSTOM\', \'INTERNET\']',
      rpz_drop_ip_rule_enabled: 'bool',
      rpz_drop_ip_rule_min_prefix_length_ipv4: 'uint',
      rpz_drop_ip_rule_min_prefix_length_ipv6: 'uint',
      rpz_qname_wait_recurse: 'bool',
      scavenging_settings: 'setting:scavenging',
      sortlist: '[{\'address\': \'string\', \'match_list\': \'string\'}]',
      use_blacklist: 'bool',
      use_ddns_force_creation_timestamp_update: 'bool',
      use_ddns_patterns_restriction: 'bool',
      use_ddns_principal_security: 'bool',
      use_ddns_restrict_protected: 'bool',
      use_ddns_restrict_static: 'bool',
      use_dns64: 'bool',
      use_dnssec: 'bool',
      use_filter_aaaa: 'bool',
      use_fixed_rrset_order_fqdns: 'bool',
      use_forwarders: 'bool',
      use_lame_ttl: 'bool',
      use_max_cache_ttl: 'bool',
      use_max_ncache_ttl: 'bool',
      use_nxdomain_redirect: 'bool',
      use_recursion: 'bool',
      use_response_rate_limiting: 'bool',
      use_root_name_server: 'bool',
      use_rpz_drop_ip_rule: 'bool',
      use_rpz_qname_wait_recurse: 'bool',
      use_scavenging_settings: 'bool',
      use_sortlist: 'bool'
    };
    describe('#createView - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createView(null, dNSCreateViewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getViewByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getViewByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateViewByReferenceBodyParam = {};
    describe('#createViewByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createViewByReference('fakedata', null, dNSCreateViewByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.run_scavenging);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createViewByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateViewByReferenceBodyParam = {
      blacklist_action: '[\'REDIRECT\', \'REFUSE\']',
      blacklist_log_query: 'bool',
      blacklist_redirect_addresses: 'string',
      blacklist_redirect_ttl: 'uint',
      blacklist_rulesets: 'string',
      comment: 'string',
      custom_root_name_servers: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      ddns_force_creation_timestamp_update: 'bool',
      ddns_principal_group: 'string',
      ddns_principal_tracking: 'bool',
      ddns_restrict_patterns: 'bool',
      ddns_restrict_patterns_list: 'string',
      ddns_restrict_protected: 'bool',
      ddns_restrict_secure: 'bool',
      ddns_restrict_static: 'bool',
      disable: 'bool',
      dns64_enabled: 'bool',
      dns64_groups: 'string',
      dnssec_enabled: 'bool',
      dnssec_expired_signatures_enabled: 'bool',
      dnssec_negative_trust_anchors: 'string',
      dnssec_trusted_keys: '[{\'fqdn\': \'string\', \'algorithm\': \'string\', \'key\': \'string\', \'secure_entry_point\': \'bool\', \'dnssec_must_be_secure\': \'bool\'}]',
      dnssec_validation_enabled: 'bool',
      enable_blacklist: 'bool',
      enable_fixed_rrset_order_fqdns: 'bool',
      enable_match_recursive_only: 'bool',
      extattrs: 'extattr',
      filter_aaaa: '[\'YES\', \'NO\', \'BREAK_DNSSEC\']',
      filter_aaaa_list: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      fixed_rrset_order_fqdns: '[{\'fqdn\': \'string\', \'record_type\': \'enum\'}]',
      forward_only: 'bool',
      forwarders: 'string',
      lame_ttl: 'uint',
      match_clients: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      match_destinations: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      max_cache_ttl: 'uint',
      max_ncache_ttl: 'uint',
      name: 'string',
      network_view: 'string',
      notify_delay: 'uint',
      nxdomain_log_query: 'bool',
      nxdomain_redirect: 'bool',
      nxdomain_redirect_addresses: 'string',
      nxdomain_redirect_addresses_v6: 'string',
      nxdomain_redirect_ttl: 'uint',
      nxdomain_rulesets: 'string',
      recursion: 'bool',
      response_rate_limiting: 'grid:responseratelimiting',
      root_name_server_type: '[\'CUSTOM\', \'INTERNET\']',
      rpz_drop_ip_rule_enabled: 'bool',
      rpz_drop_ip_rule_min_prefix_length_ipv4: 'uint',
      rpz_drop_ip_rule_min_prefix_length_ipv6: 'uint',
      rpz_qname_wait_recurse: 'bool',
      scavenging_settings: 'setting:scavenging',
      sortlist: '[{\'address\': \'string\', \'match_list\': \'string\'}]',
      use_blacklist: 'bool',
      use_ddns_force_creation_timestamp_update: 'bool',
      use_ddns_patterns_restriction: 'bool',
      use_ddns_principal_security: 'bool',
      use_ddns_restrict_protected: 'bool',
      use_ddns_restrict_static: 'bool',
      use_dns64: 'bool',
      use_dnssec: 'bool',
      use_filter_aaaa: 'bool',
      use_fixed_rrset_order_fqdns: 'bool',
      use_forwarders: 'bool',
      use_lame_ttl: 'bool',
      use_max_cache_ttl: 'bool',
      use_max_ncache_ttl: 'bool',
      use_nxdomain_redirect: 'bool',
      use_recursion: 'bool',
      use_response_rate_limiting: 'bool',
      use_root_name_server: 'bool',
      use_rpz_drop_ip_rule: 'bool',
      use_rpz_qname_wait_recurse: 'bool',
      use_scavenging_settings: 'bool',
      use_sortlist: 'bool'
    };
    describe('#updateViewByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateViewByReference('fakedata', null, dNSUpdateViewByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateViewByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteViewByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteViewByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteViewByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneAuth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneAuth(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateZoneAuthBodyParam = {
      allow_active_dir: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      allow_fixed_rrset_order: 'bool',
      allow_gss_tsig_for_underscore_zone: 'bool',
      allow_gss_tsig_zone_updates: 'bool',
      allow_query: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_transfer: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_update: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_update_forwarding: 'bool',
      comment: 'string',
      copy_xfer_to_notify: 'bool',
      create_ptr_for_bulk_hosts: 'bool',
      create_ptr_for_hosts: 'bool',
      create_underscore_zones: 'bool',
      ddns_force_creation_timestamp_update: 'bool',
      ddns_principal_group: 'string',
      ddns_principal_tracking: 'bool',
      ddns_restrict_patterns: 'bool',
      ddns_restrict_patterns_list: 'string',
      ddns_restrict_protected: 'bool',
      ddns_restrict_secure: 'bool',
      ddns_restrict_static: 'bool',
      disable: 'bool',
      disable_forwarding: 'bool',
      dns_integrity_enable: 'bool',
      dns_integrity_frequency: 'uint',
      dns_integrity_member: 'string',
      dns_integrity_verbose_logging: 'bool',
      dnssec_key_params: 'dnsseckeyparams',
      dnssec_keys: '[{\'tag\': \'uint\', \'status\': \'enum\', \'next_event_date\': \'timestamp\', \'type\': \'enum\', \'algorithm\': \'enum\', \'public_key\': \'string\'}]',
      do_host_abstraction: 'bool',
      effective_check_names_policy: '[\'FAIL\', \'WARN\']',
      extattrs: 'extattr',
      external_primaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      external_secondaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      fqdn: 'string',
      grid_primary: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      grid_secondaries: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      import_from: 'string',
      locked: 'bool',
      member_soa_mnames: '[{\'grid_primary\': \'string\', \'ms_server_primary\': \'string\', \'mname\': \'string\', \'dns_mname\': \'string\'}]',
      ms_ad_integrated: 'bool',
      ms_allow_transfer: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      ms_allow_transfer_mode: '[\'ADDRESS_AC\', \'ANY\', \'ANY_NS\', \'NONE\']',
      ms_dc_ns_record_creation: '[{\'address\': \'string\', \'comment\': \'string\'}]',
      ms_ddns_mode: '[\'ANY\', \'NONE\', \'SECURE\']',
      ms_primaries: '[{\'address\': \'string\', \'is_master\': \'bool\', \'ns_ip\': \'string\', \'ns_name\': \'string\', \'stealth\': \'bool\', \'shared_with_ms_parent_delegation\': \'bool\'}]',
      ms_secondaries: '[{\'address\': \'string\', \'is_master\': \'bool\', \'ns_ip\': \'string\', \'ns_name\': \'string\', \'stealth\': \'bool\', \'shared_with_ms_parent_delegation\': \'bool\'}]',
      ms_sync_disabled: 'bool',
      notify_delay: 'uint',
      ns_group: 'string',
      prefix: 'string',
      record_name_policy: 'string',
      restart_if_needed: 'bool',
      scavenging_settings: 'setting:scavenging',
      set_soa_serial_number: 'bool',
      soa_default_ttl: 'uint',
      soa_email: 'string',
      soa_expire: 'uint',
      soa_negative_ttl: 'uint',
      soa_refresh: 'uint',
      soa_retry: 'uint',
      soa_serial_number: 'uint',
      srgs: 'string',
      update_forwarding: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      use_allow_active_dir: 'bool',
      use_allow_query: 'bool',
      use_allow_transfer: 'bool',
      use_allow_update: 'bool',
      use_allow_update_forwarding: 'bool',
      use_check_names_policy: 'bool',
      use_copy_xfer_to_notify: 'bool',
      use_ddns_force_creation_timestamp_update: 'bool',
      use_ddns_patterns_restriction: 'bool',
      use_ddns_principal_security: 'bool',
      use_ddns_restrict_protected: 'bool',
      use_ddns_restrict_static: 'bool',
      use_dnssec_key_params: 'bool',
      use_external_primary: 'bool',
      use_grid_zone_timer: 'bool',
      use_import_from: 'bool',
      use_notify_delay: 'bool',
      use_record_name_policy: 'bool',
      use_scavenging_settings: 'bool',
      use_soa_email: 'bool',
      view: 'string',
      zone_format: '[\'FORWARD\', \'IPV4\', \'IPV6\']'
    };
    describe('#createZoneAuth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createZoneAuth(null, dNSCreateZoneAuthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createZoneAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateZoneAuthByReferenceBodyParam = {
      allow_active_dir: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      allow_fixed_rrset_order: 'bool',
      allow_gss_tsig_for_underscore_zone: 'bool',
      allow_gss_tsig_zone_updates: 'bool',
      allow_query: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_transfer: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_update: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      allow_update_forwarding: 'bool',
      comment: 'string',
      copy_xfer_to_notify: 'bool',
      create_ptr_for_bulk_hosts: 'bool',
      create_ptr_for_hosts: 'bool',
      create_underscore_zones: 'bool',
      ddns_force_creation_timestamp_update: 'bool',
      ddns_principal_group: 'string',
      ddns_principal_tracking: 'bool',
      ddns_restrict_patterns: 'bool',
      ddns_restrict_patterns_list: 'string',
      ddns_restrict_protected: 'bool',
      ddns_restrict_secure: 'bool',
      ddns_restrict_static: 'bool',
      disable: 'bool',
      disable_forwarding: 'bool',
      dns_integrity_enable: 'bool',
      dns_integrity_frequency: 'uint',
      dns_integrity_member: 'string',
      dns_integrity_verbose_logging: 'bool',
      dnssec_key_params: 'dnsseckeyparams',
      dnssec_keys: '[{\'tag\': \'uint\', \'status\': \'enum\', \'next_event_date\': \'timestamp\', \'type\': \'enum\', \'algorithm\': \'enum\', \'public_key\': \'string\'}]',
      do_host_abstraction: 'bool',
      effective_check_names_policy: '[\'FAIL\', \'WARN\']',
      extattrs: 'extattr',
      external_primaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      external_secondaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      grid_primary: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      grid_secondaries: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      import_from: 'string',
      locked: 'bool',
      member_soa_mnames: '[{\'grid_primary\': \'string\', \'ms_server_primary\': \'string\', \'mname\': \'string\', \'dns_mname\': \'string\'}]',
      ms_ad_integrated: 'bool',
      ms_allow_transfer: '[{\'address\': \'string\', \'permission\': \'enum\'}]',
      ms_allow_transfer_mode: '[\'ADDRESS_AC\', \'ANY\', \'ANY_NS\', \'NONE\']',
      ms_dc_ns_record_creation: '[{\'address\': \'string\', \'comment\': \'string\'}]',
      ms_ddns_mode: '[\'ANY\', \'NONE\', \'SECURE\']',
      ms_primaries: '[{\'address\': \'string\', \'is_master\': \'bool\', \'ns_ip\': \'string\', \'ns_name\': \'string\', \'stealth\': \'bool\', \'shared_with_ms_parent_delegation\': \'bool\'}]',
      ms_secondaries: '[{\'address\': \'string\', \'is_master\': \'bool\', \'ns_ip\': \'string\', \'ns_name\': \'string\', \'stealth\': \'bool\', \'shared_with_ms_parent_delegation\': \'bool\'}]',
      ms_sync_disabled: 'bool',
      notify_delay: 'uint',
      ns_group: 'string',
      prefix: 'string',
      record_name_policy: 'string',
      restart_if_needed: 'bool',
      scavenging_settings: 'setting:scavenging',
      set_soa_serial_number: 'bool',
      soa_default_ttl: 'uint',
      soa_email: 'string',
      soa_expire: 'uint',
      soa_negative_ttl: 'uint',
      soa_refresh: 'uint',
      soa_retry: 'uint',
      soa_serial_number: 'uint',
      srgs: 'string',
      update_forwarding: '[{\'address\': \'string\', \'permission\': \'enum\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      use_allow_active_dir: 'bool',
      use_allow_query: 'bool',
      use_allow_transfer: 'bool',
      use_allow_update: 'bool',
      use_allow_update_forwarding: 'bool',
      use_check_names_policy: 'bool',
      use_copy_xfer_to_notify: 'bool',
      use_ddns_force_creation_timestamp_update: 'bool',
      use_ddns_patterns_restriction: 'bool',
      use_ddns_principal_security: 'bool',
      use_ddns_restrict_protected: 'bool',
      use_ddns_restrict_static: 'bool',
      use_dnssec_key_params: 'bool',
      use_external_primary: 'bool',
      use_grid_zone_timer: 'bool',
      use_import_from: 'bool',
      use_notify_delay: 'bool',
      use_record_name_policy: 'bool',
      use_scavenging_settings: 'bool',
      use_soa_email: 'bool',
      view: 'string'
    };
    describe('#updateZoneAuthByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateZoneAuthByReference('fakedata', null, dNSUpdateZoneAuthByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateZoneAuthByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneAuthDiscrepancy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneAuthDiscrepancy(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneAuthDiscrepancy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneAuthDiscrepancyByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneAuthDiscrepancyByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneAuthDiscrepancyByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneDelegatedByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneDelegatedByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneDelegatedByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateZoneDelegatedByReferenceBodyParam = {};
    describe('#createZoneDelegatedByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createZoneDelegatedByReference('fakedata', null, dNSCreateZoneDelegatedByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.lock_unlock_zone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createZoneDelegatedByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateZoneDelegatedByReferenceBodyParam = {
      comment: 'string',
      delegate_to: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      delegated_ttl: 'uint',
      disable: 'bool',
      enable_rfc2317_exclusion: 'bool',
      extattrs: 'extattr',
      locked: 'bool',
      ms_ad_integrated: 'bool',
      ms_ddns_mode: '[\'ANY\', \'NONE\', \'SECURE\']',
      ns_group: 'string',
      prefix: 'string',
      use_delegated_ttl: 'bool',
      view: 'string'
    };
    describe('#updateZoneDelegatedByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateZoneDelegatedByReference('fakedata', null, dNSUpdateZoneDelegatedByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateZoneDelegatedByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneDelegatedByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZoneDelegatedByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteZoneDelegatedByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneForwardByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneForwardByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneForwardByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateZoneForwardByReferenceBodyParam = {};
    describe('#createZoneForwardByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createZoneForwardByReference('fakedata', null, dNSCreateZoneForwardByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.lock_unlock_zone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createZoneForwardByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateZoneForwardByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      disable_ns_generation: 'bool',
      extattrs: 'extattr',
      external_ns_group: 'string',
      forward_to: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      forwarders_only: 'bool',
      forwarding_servers: '[{\'name\': \'string\', \'forwarders_only\': \'bool\', \'forward_to\': \'extserver\', \'use_override_forwarders\': \'bool\'}]',
      locked: 'bool',
      ms_ad_integrated: 'bool',
      ms_ddns_mode: '[\'ANY\', \'NONE\', \'SECURE\']',
      ns_group: 'string',
      prefix: 'string',
      view: 'string'
    };
    describe('#updateZoneForwardByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateZoneForwardByReference('fakedata', null, dNSUpdateZoneForwardByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateZoneForwardByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneForwardByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZoneForwardByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteZoneForwardByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneRpByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneRpByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneRpByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateZoneRpByReferenceBodyParam = {};
    describe('#createZoneRpByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createZoneRpByReference('fakedata', null, dNSCreateZoneRpByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.copy_rpz_records);
                assert.equal('object', typeof data.response.lock_unlock_zone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createZoneRpByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateZoneRpByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      extattrs: 'extattr',
      external_primaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      external_secondaries: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      fireeye_rule_mapping: 'fireeye:rulemapping',
      grid_primary: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      grid_secondaries: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      locked: 'bool',
      log_rpz: 'bool',
      member_soa_mnames: '[{\'grid_primary\': \'string\', \'ms_server_primary\': \'string\', \'mname\': \'string\', \'dns_mname\': \'string\'}]',
      ns_group: 'string',
      prefix: 'string',
      record_name_policy: 'string',
      rpz_drop_ip_rule_enabled: 'bool',
      rpz_drop_ip_rule_min_prefix_length_ipv4: 'uint',
      rpz_drop_ip_rule_min_prefix_length_ipv6: 'uint',
      rpz_policy: '[\'DISABLED\', \'GIVEN\', \'NODATA\', \'NXDOMAIN\', \'PASSTHRU\', \'SUBSTITUTE\']',
      rpz_severity: '[\'CRITICAL\', \'MAJOR\', \'WARNING\', \'INFORMATIONAL\']',
      set_soa_serial_number: 'bool',
      soa_default_ttl: 'uint',
      soa_email: 'string',
      soa_expire: 'uint',
      soa_negative_ttl: 'uint',
      soa_refresh: 'uint',
      soa_retry: 'uint',
      soa_serial_number: 'uint',
      substitute_name: 'string',
      use_external_primary: 'bool',
      use_grid_zone_timer: 'bool',
      use_log_rpz: 'bool',
      use_record_name_policy: 'bool',
      use_rpz_drop_ip_rule: 'bool',
      use_soa_email: 'bool',
      view: 'string'
    };
    describe('#updateZoneRpByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateZoneRpByReference('fakedata', null, dNSUpdateZoneRpByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateZoneRpByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneRpByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZoneRpByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteZoneRpByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneStub - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneStub(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneStub', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateZoneStubBodyParam = {
      comment: 'string',
      disable: 'bool',
      disable_forwarding: 'bool',
      extattrs: 'extattr',
      external_ns_group: 'string',
      fqdn: 'string',
      locked: 'bool',
      ms_ad_integrated: 'bool',
      ms_ddns_mode: '[\'ANY\', \'NONE\', \'SECURE\']',
      ns_group: 'string',
      prefix: 'string',
      stub_from: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      stub_members: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      stub_msservers: '[{\'address\': \'string\', \'is_master\': \'bool\', \'ns_ip\': \'string\', \'ns_name\': \'string\', \'stealth\': \'bool\', \'shared_with_ms_parent_delegation\': \'bool\'}]',
      view: 'string',
      zone_format: '[\'FORWARD\', \'IPV4\', \'IPV6\']'
    };
    describe('#createZoneStub - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createZoneStub(null, dNSCreateZoneStubBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createZoneStub', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneStubByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZoneStubByReference('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'getZoneStubByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSCreateZoneStubByReferenceBodyParam = {};
    describe('#createZoneStubByReference - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createZoneStubByReference('fakedata', null, dNSCreateZoneStubByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.lock_unlock_zone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'createZoneStubByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dNSUpdateZoneStubByReferenceBodyParam = {
      comment: 'string',
      disable: 'bool',
      disable_forwarding: 'bool',
      extattrs: 'extattr',
      external_ns_group: 'string',
      locked: 'bool',
      ms_ad_integrated: 'bool',
      ms_ddns_mode: '[\'ANY\', \'NONE\', \'SECURE\']',
      ns_group: 'string',
      prefix: 'string',
      stub_from: '[{\'address\': \'string\', \'name\': \'string\', \'shared_with_ms_parent_delegation\': \'bool\', \'stealth\': \'bool\', \'tsig_key\': \'string\', \'tsig_key_alg\': \'enum\', \'tsig_key_name\': \'string\', \'use_tsig_key_name\': \'bool\'}]',
      stub_members: '[{\'name\': \'string\', \'stealth\': \'bool\', \'grid_replicate\': \'bool\', \'lead\': \'bool\', \'preferred_primaries\': \'extserver\', \'enable_preferred_primaries\': \'bool\'}]',
      stub_msservers: '[{\'address\': \'string\', \'is_master\': \'bool\', \'ns_ip\': \'string\', \'ns_name\': \'string\', \'stealth\': \'bool\', \'shared_with_ms_parent_delegation\': \'bool\'}]',
      view: 'string'
    };
    describe('#updateZoneStubByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateZoneStubByReference('fakedata', null, dNSUpdateZoneStubByReferenceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'updateZoneStubByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZoneStubByReference - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteZoneStubByReference('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DNS', 'deleteZoneStubByReference', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkZoneAssociation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNetworkZoneAssociation('fakedata/16', null, null, { key: 'value' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'addNetworkZoneAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkZoneAssociations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkZoneAssociations('fakedata/16', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-infoblox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getNetworkZoneAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
