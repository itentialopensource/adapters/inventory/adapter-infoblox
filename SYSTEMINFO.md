# Infoblox

Vendor: Infoblox
Homepage: https://www.infoblox.com/

Product: Infoblox
Product Page: https://www.infoblox.com/

## Introduction
We classify Infoblox into the Inventory domain because it provides the management and organization of network assets.

"Infoblox helps deliver DNS services across physical, virtual and cloud environments."

## Why Integrate
The Infoblox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Infoblox. With this adapter you have the ability to perform operations such as:

- Add and Remove Inventory Items
- Check Availability of Items
- Assign and Unassign Items

## Additional Product Documentation
The [API documents for Infoblox](https://www.infoblox.com/wp-content/uploads/infoblox-deployment-infoblox-rest-api.pdf)