## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Infoblox. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Infoblox.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Infoblox. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(network, comment, callback)</td>
    <td style="padding:15px">createNetwork</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkContainer(payload, callback)</td>
    <td style="padding:15px">createNetworkContainer</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthZone(fqdnName, callback)</td>
    <td style="padding:15px">createAuthZone</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHostRecord(hostName, fqdnName, IPAddress, callback)</td>
    <td style="padding:15px">createHostRecord</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHostRecord2(hostName, comment, IPAddress, callback)</td>
    <td style="padding:15px">createHostRecord2</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDetailsWithQuery(networkIP, query, callback)</td>
    <td style="padding:15px">getNetworkDetails</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDetailsByFilter(filter, callback)</td>
    <td style="padding:15px">getNetworkDetailsByFilter</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkContainerDetailsWithQuery(networkIP, query, callback)</td>
    <td style="padding:15px">getNetworkContainerDetails</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkContainers(query, callback)</td>
    <td style="padding:15px">listNetworkContainers</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthZoneDetailsByfqdnNameWithQuery(fqdnName, query, callback)</td>
    <td style="padding:15px">getAuthZoneDetailsByfqdnName</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthZone(fqdnName, callback)</td>
    <td style="padding:15px">deleteAuthZone</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(networkIP, callback)</td>
    <td style="padding:15px">delete network via the container</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkv2(networkIP, callback)</td>
    <td style="padding:15px">delete only the network</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkKeyByIPWithQuery(networkIP, query, callback)</td>
    <td style="padding:15px">getNetworkKeyByIP</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableNetworksWithQuery(networkId, subnetMask, query, callback)</td>
    <td style="padding:15px">getNextAvailableNetworksWithQuery</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableIpsWithQuery(networkRef, number, query, callback)</td>
    <td style="padding:15px">getNextAvailableIpsWithQuery</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpDetailsWithQuery(IPAddress, query, callback)</td>
    <td style="padding:15px">getIpDetails</td>
    <td style="padding:15px">{base_path}/{version}/ipv4address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpByHostWithQuery(hostName, query, callback)</td>
    <td style="padding:15px">getIpByHost</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostByIpWithQuery(IPAddress, query, callback)</td>
    <td style="padding:15px">getHostByIp</td>
    <td style="padding:15px">{base_path}/{version}/ipv4address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostKeyByHostNameWithQuery(hostName, query, callback)</td>
    <td style="padding:15px">getHostKeyByHostName</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostKeysByFilter(filter, callback)</td>
    <td style="padding:15px">getHostKeysByFilter</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkUtilizationByIPWithQuery(networkIP, query, callback)</td>
    <td style="padding:15px">getNetworkUtilizationByIP</td>
    <td style="padding:15px">{base_path}/{version}/ipam:statistics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkContainerByNetworkKey(networkId, callback)</td>
    <td style="padding:15px">deleteNetworkContainerByNetworkKey</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkContainerNextNetworkIps(networkId, containerId, body, query, callback)</td>
    <td style="padding:15px">This function will get the next network ips from the container</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpv6NetworkContainerNextNetworkIps(networkId, containerId, body, query, callback)</td>
    <td style="padding:15px">This function will get the next network ips from the container</td>
    <td style="padding:15px">{base_path}/{version}/ipv6networkcontainer/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostRecordByHostName(hostName, callback)</td>
    <td style="padding:15px">deleteHostRecordByHostName</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkByNetworkKey(networkId, callback)</td>
    <td style="padding:15px">deleteNetworkByNetworkKey</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignNextNetwork(networkBlock, nextNetworkSubnetMask, comment, callback)</td>
    <td style="padding:15px">assignNextNetwork</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignNextNetworkByNetwork(networkBlock, nextNetworkSubnetMask, networkView, returnFields, comment, callback)</td>
    <td style="padding:15px">assignNextNetworkByNetwork</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignNextNetworkByRef(networkReference, nextNetworkSubnetMask, networkView, returnFields, comment, callback)</td>
    <td style="padding:15px">assignNextNetworkByRef</td>
    <td style="padding:15px">{base_path}/{version}/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignIP(IPAddress, callback)</td>
    <td style="padding:15px">unassignIP</td>
    <td style="padding:15px">{base_path}/{version}/ipv4address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyNetworkBlock(networkIP, comment, callback)</td>
    <td style="padding:15px">modifyNetworkBlock</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForwardZonesWithQuery(query, callback)</td>
    <td style="padding:15px">GET Forward Zones</td>
    <td style="padding:15px">{base_path}/{version}/zone_forward?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createForwardZone(body, callback)</td>
    <td style="padding:15px">Creates (POST) Forward Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone_forward?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZones(objectReference, callback)</td>
    <td style="padding:15px">DELETE Zones</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneDelegationsWithQuery(query, callback)</td>
    <td style="padding:15px">GET Zone Delegations</td>
    <td style="padding:15px">{base_path}/{version}/zone_delegated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneDelegation(body, callback)</td>
    <td style="padding:15px">Creates (POST) Zone Delegation</td>
    <td style="padding:15px">{base_path}/{version}/zone_delegated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthZoneByRef(zoneRef, callback)</td>
    <td style="padding:15px">This function will delete the auth zone by reference</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkBlockWithQuery(objectReference, query, callback)</td>
    <td style="padding:15px">GET Network block associations</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkContainer(networkRef, callback)</td>
    <td style="padding:15px">This function will delete the network container by network key</td>
    <td style="padding:15px">{base_path}/{version}/networkcontainer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericCreateNextAvailableNetwork(networkRef, options, body, callback)</td>
    <td style="padding:15px">Creates the next available network in a network container by Ref</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkBlock(functionParam, objectReference, body, callback)</td>
    <td style="padding:15px">POST Grid Service status</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDtcServerWithQuery(query, callback)</td>
    <td style="padding:15px">GET DTC Server</td>
    <td style="padding:15px">{base_path}/{version}/dtc:server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDtcServer(body, callback)</td>
    <td style="padding:15px">Creates (POST) DTC Server</td>
    <td style="padding:15px">{base_path}/{version}/dtc:server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDtcPoolWithQuery(query, callback)</td>
    <td style="padding:15px">GET DTC Pool</td>
    <td style="padding:15px">{base_path}/{version}/dtc:pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDtcPool(body, callback)</td>
    <td style="padding:15px">Creates (POST) DTC Pool</td>
    <td style="padding:15px">{base_path}/{version}/dtc:pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDtcLbdn(body, callback)</td>
    <td style="padding:15px">Creates (POST) DTC LBDN</td>
    <td style="padding:15px">{base_path}/{version}/dtc:lbdn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostRecordWithQuery(hostName, query, callback)</td>
    <td style="padding:15px">GET HOST record by name</td>
    <td style="padding:15px">{base_path}/{version}/record:host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getARecordsWithQuery(name, query, callback)</td>
    <td style="padding:15px">get A records</td>
    <td style="padding:15px">{base_path}/{version}/record:a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createARecord(body, callback)</td>
    <td style="padding:15px">Create A Record</td>
    <td style="padding:15px">{base_path}/{version}/record:a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCnameRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET CNAME Records</td>
    <td style="padding:15px">{base_path}/{version}/record:cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCNAMERecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) CNAME Record</td>
    <td style="padding:15px">{base_path}/{version}/record:cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTxtRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET TXT Records</td>
    <td style="padding:15px">{base_path}/{version}/record:txt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTxtRecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) TXT Record</td>
    <td style="padding:15px">{base_path}/{version}/record:txt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRecordsWithQuery(zone, query, callback)</td>
    <td style="padding:15px">GET All Records</td>
    <td style="padding:15px">{base_path}/{version}/allrecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAaaaRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET AAAA Records</td>
    <td style="padding:15px">{base_path}/{version}/record:aaaa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAaaaRecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) AAAA Record</td>
    <td style="padding:15px">{base_path}/{version}/record:aaaa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMxRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET MX Records</td>
    <td style="padding:15px">{base_path}/{version}/record:mx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMxRecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) MX Record</td>
    <td style="padding:15px">{base_path}/{version}/record:mx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET NS Records</td>
    <td style="padding:15px">{base_path}/{version}/record:ns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNsRecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) NS Record</td>
    <td style="padding:15px">{base_path}/{version}/record:ns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPtrRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET PTR Records</td>
    <td style="padding:15px">{base_path}/{version}/record:ptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPtrRecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) PTR Record</td>
    <td style="padding:15px">{base_path}/{version}/record:ptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrvRecordsWithQuery(query, callback)</td>
    <td style="padding:15px">GET SRV Records</td>
    <td style="padding:15px">{base_path}/{version}/record:srv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSrvRecord(body, callback)</td>
    <td style="padding:15px">Creates (POST) SRV Record</td>
    <td style="padding:15px">{base_path}/{version}/record:srv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHostRecord(recordkey, body, callback)</td>
    <td style="padding:15px">Updates a record</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostRecord(recordkey, callback)</td>
    <td style="padding:15px">Deletes the record</td>
    <td style="padding:15px">{base_path}/{version}/record:host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateARecord(recordkey, body, callback)</td>
    <td style="padding:15px">Updates a record</td>
    <td style="padding:15px">{base_path}/{version}/record:a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteARecord(recordkey, callback)</td>
    <td style="padding:15px">Deletes the record</td>
    <td style="padding:15px">{base_path}/{version}/record:a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePTRRecord(recordkey, body, callback)</td>
    <td style="padding:15px">Updates a record</td>
    <td style="padding:15px">{base_path}/{version}/record:ptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePTRRecord(recordkey, callback)</td>
    <td style="padding:15px">Deletes the record</td>
    <td style="padding:15px">{base_path}/{version}/record:ptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCNAMERecord(recordkey, body, callback)</td>
    <td style="padding:15px">Updates a record</td>
    <td style="padding:15px">{base_path}/{version}/record:cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCNAMERecord(recordkey, callback)</td>
    <td style="padding:15px">Deletes the record</td>
    <td style="padding:15px">{base_path}/{version}/record:cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGridDnsWithQuery(query, callback)</td>
    <td style="padding:15px">GET grid:dns</td>
    <td style="padding:15px">{base_path}/{version}/grid:dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMemberDnsWithQuery(query, callback)</td>
    <td style="padding:15px">GET member:dns</td>
    <td style="padding:15px">{base_path}/{version}/member:dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResponsePolicyZonesWithQuery(query, callback)</td>
    <td style="padding:15px">GET Response Policy Zones</td>
    <td style="padding:15px">{base_path}/{version}/zone_rp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResponsePolicyZone(body, callback)</td>
    <td style="padding:15px">Creates (POST) Response Policy Zone</td>
    <td style="padding:15px">{base_path}/{version}/zone_rp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubstituitionRuleForARecords(body, callback)</td>
    <td style="padding:15px">Creates (POST) Substituition rule for A records</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubstituitionRuleForPtrRecords(body, callback)</td>
    <td style="padding:15px">POST Substituition rule for PTR records</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:ptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubstituitionRuleForIpTriggerPolicy(body, callback)</td>
    <td style="padding:15px">Creates (POST) Substituition rule for IP trigger policy</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:a:ipaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBlockDomainNameRule(body, callback)</td>
    <td style="padding:15px">Creates (POST) Block Domain Name Rule</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBlockClientIpAddressRule(body, callback)</td>
    <td style="padding:15px">Creates (POST) Block Client IP Address Rule</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:cname:clientipaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubstituteDomainNameClientIpAddressRule(body, callback)</td>
    <td style="padding:15px">Creates (POST) Substitute Domain Name Rule</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:cname:clientipaddressdn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBlockIpAddressNoSuchDomainRule(body, callback)</td>
    <td style="padding:15px">Creates (POST) Block IP Address Rule</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:cname:ipaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubstituteDomainNameIpAddressRule(body, callback)</td>
    <td style="padding:15px">Creates (POST) Substitute Domain Name Rule</td>
    <td style="padding:15px">{base_path}/{version}/record:rpz:cname:ipaddressdn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRpzRecordsWithQuery(zone, query, callback)</td>
    <td style="padding:15px">GET All RPZ Records</td>
    <td style="padding:15px">{base_path}/{version}/allrpzrecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNameServerGroupsWithQuery(query, callback)</td>
    <td style="padding:15px">GET Name Server Groups</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNameServerGroup(body, callback)</td>
    <td style="padding:15px">Creates (POST) Name Server Group</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRangeByExtensibleAttributeWithQuery(location, query, callback)</td>
    <td style="padding:15px">GET Range by Extensible Attribute</td>
    <td style="padding:15px">{base_path}/{version}/range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRange(body, callback)</td>
    <td style="padding:15px">Creates (POST) Range</td>
    <td style="padding:15px">{base_path}/{version}/range?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLeaseByIpAddressWithQuery(address, query, callback)</td>
    <td style="padding:15px">GET Lease by IP address</td>
    <td style="padding:15px">{base_path}/{version}/lease?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMemberDhcpWithQuery(query, callback)</td>
    <td style="padding:15px">GET member:dhcp</td>
    <td style="padding:15px">{base_path}/{version}/member:dhcpproperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpAddressUsingSearchWithQuery(address, query, callback)</td>
    <td style="padding:15px">GET IP address using search</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsViewWithQuery(query, callback)</td>
    <td style="padding:15px">GET DNS view</td>
    <td style="padding:15px">{base_path}/{version}/view?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkViewWithQuery(query, callback)</td>
    <td style="padding:15px">GET Network view with Query Param</td>
    <td style="padding:15px">{base_path}/{version}/networkview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkView(body, callback)</td>
    <td style="padding:15px">Creates (POST) Network View</td>
    <td style="padding:15px">{base_path}/{version}/networkview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkViewById(viewId, query, callback)</td>
    <td style="padding:15px">GET Network view by id</td>
    <td style="padding:15px">{base_path}/{version}/networkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkView(viewId, callback)</td>
    <td style="padding:15px">Update Network view</td>
    <td style="padding:15px">{base_path}/{version}/networkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkView(viewId, callback)</td>
    <td style="padding:15px">Delete Network view</td>
    <td style="padding:15px">{base_path}/{version}/networkview/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFixedAddressMacWithQuery(mac, query, callback)</td>
    <td style="padding:15px">GET Fixed address MAC</td>
    <td style="padding:15px">{base_path}/{version}/fixedaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFixedAddress(body, callback)</td>
    <td style="padding:15px">Creates (POST) Fixed Address</td>
    <td style="padding:15px">{base_path}/{version}/fixedaddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMembersWithQuery(query, callback)</td>
    <td style="padding:15px">GET Members</td>
    <td style="padding:15px">{base_path}/{version}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMember(body, callback)</td>
    <td style="padding:15px">Creates (POST) Member</td>
    <td style="padding:15px">{base_path}/{version}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGridWithQuery(query, callback)</td>
    <td style="padding:15px">GET grid</td>
    <td style="padding:15px">{base_path}/{version}/grid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGridStatusWithQuery(query, callback)</td>
    <td style="padding:15px">GET grid status</td>
    <td style="padding:15px">{base_path}/{version}/grid:servicerestart:status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGridPendingChangesWithQuery(query, callback)</td>
    <td style="padding:15px">GET grid pending changes</td>
    <td style="padding:15px">{base_path}/{version}/grid:servicerestart:request:changedobject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVdiscoveryTasksWithQuery(query, callback)</td>
    <td style="padding:15px">GET vDiscovery tasks</td>
    <td style="padding:15px">{base_path}/{version}/vdiscoverytask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVdiscoveryTask(body, callback)</td>
    <td style="padding:15px">Creates (POST) vDiscovery task</td>
    <td style="padding:15px">{base_path}/{version}/vdiscoverytask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissionsForARoleWithQuery(role, query, callback)</td>
    <td style="padding:15px">GET Permissions for a role</td>
    <td style="padding:15px">{base_path}/{version}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiSchemaWithQuery(schema, query, callback)</td>
    <td style="padding:15px">GET WAPI Schema</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multipleRecordTypes(body, callback)</td>
    <td style="padding:15px">Multiple record types</td>
    <td style="padding:15px">{base_path}/{version}/request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensibleAttributeDefinitionWithQuery(query, callback)</td>
    <td style="padding:15px">GET Extensible Attribute definition With Query Params</td>
    <td style="padding:15px">{base_path}/{version}/extensibleattributedef?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensibleAttributeDefinition(body, callback)</td>
    <td style="padding:15px">Creates (POST) Extensible Attribute Definition</td>
    <td style="padding:15px">{base_path}/{version}/extensibleattributedef?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensibleAttributeDefinitionById(eaId, query, callback)</td>
    <td style="padding:15px">GET Extensible Attribute definition By Id</td>
    <td style="padding:15px">{base_path}/{version}/extensibleattributedef/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExtensibleAttributeDefinition(eaId, callback)</td>
    <td style="padding:15px">Update Extensible Attribute definition</td>
    <td style="padding:15px">{base_path}/{version}/extensibleattributedef/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(urlFromUploadInitiation, filedata, callback)</td>
    <td style="padding:15px">Uploads a file to infoblox</td>
    <td style="padding:15px">/http_direct_file_io/{pathv1}/import_records</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadFile(urlFromDownloadInitiation, callback)</td>
    <td style="padding:15px">Downloads a file from infoblox</td>
    <td style="padding:15px">/http_direct_file_io/{pathv1}/{pathv2}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensibleAttributeDefinition(eaId, callback)</td>
    <td style="padding:15px">Delete Extensible Attribute definition</td>
    <td style="padding:15px">{base_path}/{version}/extensibleattributedef/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObject(objectType, body, callback)</td>
    <td style="padding:15px">createObject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObject(objectReference, body, callback)</td>
    <td style="padding:15px">updateObject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObject(objectReference, callback)</td>
    <td style="padding:15px">deleteObject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObject(objectType, queryObject, returnFields, callback)</td>
    <td style="padding:15px">getObject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartServices(objectReference, queries, callback)</td>
    <td style="padding:15px">POST restartServices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}:{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRecordsByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getAllRecordsByReference</td>
    <td style="padding:15px">{base_path}/{version}/allrecords/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBulkhosts(query, callback)</td>
    <td style="padding:15px">getBulkhosts</td>
    <td style="padding:15px">{base_path}/{version}/bulkhost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkhosts(query, body, callback)</td>
    <td style="padding:15px">createBulkhosts</td>
    <td style="padding:15px">{base_path}/{version}/bulkhost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBulkhostByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getBulkhostByReference</td>
    <td style="padding:15px">{base_path}/{version}/bulkhost/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBulkhostByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateBulkhostByReference</td>
    <td style="padding:15px">{base_path}/{version}/bulkhost/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBulkhostByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteBulkhostByReference</td>
    <td style="padding:15px">{base_path}/{version}/bulkhost/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBulkhostTemplate(query, callback)</td>
    <td style="padding:15px">getBulkhostTemplate</td>
    <td style="padding:15px">{base_path}/{version}/bulkhostnametemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkhostTemplate(query, body, callback)</td>
    <td style="padding:15px">createBulkhostTemplate</td>
    <td style="padding:15px">{base_path}/{version}/bulkhostnametemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBulkhostTemplateByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getBulkhostTemplateByReference</td>
    <td style="padding:15px">{base_path}/{version}/bulkhostnametemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBulkhostTemplateByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateBulkhostTemplateByReference</td>
    <td style="padding:15px">{base_path}/{version}/bulkhostnametemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBulkhostTemplateByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteBulkhostTemplateByReference</td>
    <td style="padding:15px">{base_path}/{version}/bulkhostnametemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDdnsPrincipalCluster(query, callback)</td>
    <td style="padding:15px">getDdnsPrincipalCluster</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDdnsPrincipalCluster(query, body, callback)</td>
    <td style="padding:15px">createDdnsPrincipalCluster</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDdnsPrincipalClusterByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDdnsPrincipalClusterByReference</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDdnsPrincipalClusterByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateDdnsPrincipalClusterByReference</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDdnsPrincipalClusterByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteDdnsPrincipalClusterByReference</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDdnsPrincipalClusterGroup(query, callback)</td>
    <td style="padding:15px">getDdnsPrincipalClusterGroup</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster:group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDdnsPrincipalClusterGroup(query, body, callback)</td>
    <td style="padding:15px">createDdnsPrincipalClusterGroup</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster:group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDdnsPrincipalClusterGroupByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDdnsPrincipalClusterGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster:group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDdnsPrincipalClusterGroupByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateDdnsPrincipalClusterGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster:group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDdnsPrincipalClusterGroupByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteDdnsPrincipalClusterGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/ddns:principalcluster:group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDns64Group(query, callback)</td>
    <td style="padding:15px">getDns64Group</td>
    <td style="padding:15px">{base_path}/{version}/dns64group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDns64Group(query, body, callback)</td>
    <td style="padding:15px">createDns64Group</td>
    <td style="padding:15px">{base_path}/{version}/dns64group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDns64GroupByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDns64GroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/dns64group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDns64GroupByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateDns64GroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/dns64group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDns64GroupByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteDns64GroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/dns64group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGridDnsByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getGridDnsByReference</td>
    <td style="padding:15px">{base_path}/{version}/grid:dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGridDnsByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">createGridDnsByReference</td>
    <td style="padding:15px">{base_path}/{version}/grid:dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGridDnsByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateGridDnsByReference</td>
    <td style="padding:15px">{base_path}/{version}/grid:dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostnameRewritePolicy(query, callback)</td>
    <td style="padding:15px">getHostnameRewritePolicy</td>
    <td style="padding:15px">{base_path}/{version}/hostnamerewritepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostnameRewritePolicyByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getHostnameRewritePolicyByReference</td>
    <td style="padding:15px">{base_path}/{version}/hostnamerewritepolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMemberDnsByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getMemberDnsByReference</td>
    <td style="padding:15px">{base_path}/{version}/member:dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMemberDnsByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">createMemberDnsByReference</td>
    <td style="padding:15px">{base_path}/{version}/member:dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMemberDnsByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateMemberDnsByReference</td>
    <td style="padding:15px">{base_path}/{version}/member:dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNsGroupByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNsGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsGroupByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNsGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupDelegation(query, callback)</td>
    <td style="padding:15px">getNsGroupDelegation</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:delegation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNsGroupDelegation(query, body, callback)</td>
    <td style="padding:15px">createNsGroupDelegation</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:delegation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupDelegationByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsGroupDelegationByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:delegation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNsGroupDelegationByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNsGroupDelegationByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:delegation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsGroupDelegationByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNsGroupDelegationByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:delegation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupForwardMember(query, callback)</td>
    <td style="padding:15px">getNsGroupForwardMember</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardingmember?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNsGroupForwardMember(query, body, callback)</td>
    <td style="padding:15px">createNsGroupForwardMember</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardingmember?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupForwardMemberByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsGroupForwardMemberByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardingmember/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNsGroupForwardMemberByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNsGroupForwardMemberByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardingmember/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsGroupForwardMemberByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNsGroupForwardMemberByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardingmember/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupForwardStubServer(query, callback)</td>
    <td style="padding:15px">getNsGroupForwardStubServer</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardstubserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNsGroupForwardStubServer(query, body, callback)</td>
    <td style="padding:15px">createNsGroupForwardStubServer</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardstubserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupForwardStubServerByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsGroupForwardStubServerByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardstubserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNsGroupForwardStubServerByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNsGroupForwardStubServerByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardstubserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsGroupForwardStubServerByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNsGroupForwardStubServerByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:forwardstubserver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupStubmember(query, callback)</td>
    <td style="padding:15px">getNsGroupStubmember</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:stubmember?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNsGroupStubmember(query, body, callback)</td>
    <td style="padding:15px">createNsGroupStubmember</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:stubmember?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsGroupStubmemberByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsGroupStubmemberByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:stubmember/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNsGroupStubmemberByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNsGroupStubmemberByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:stubmember/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsGroupStubmemberByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNsGroupStubmemberByReference</td>
    <td style="padding:15px">{base_path}/{version}/nsgroup:stubmember/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAAAArecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getAAAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAAAArecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateAAAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAAAArecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteAAAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAliasrecord(query, callback)</td>
    <td style="padding:15px">getAliasrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:alias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAliasrecord(query, body, callback)</td>
    <td style="padding:15px">createAliasrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:alias?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAliasrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getAliasrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:alias/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAliasrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateAliasrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:alias/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAliasrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteAliasrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:alias/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCAArecord(query, callback)</td>
    <td style="padding:15px">getCAArecord</td>
    <td style="padding:15px">{base_path}/{version}/record:caa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCAArecord(query, body, callback)</td>
    <td style="padding:15px">createCAArecord</td>
    <td style="padding:15px">{base_path}/{version}/record:caa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCAArecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getCAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:caa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCAArecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateCAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:caa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCAArecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteCAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:caa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCnamerecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getCnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcidrecord(query, callback)</td>
    <td style="padding:15px">getDhcidrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:dhcid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDhcidrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDhcidrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dhcid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcidrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteDhcidrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dhcid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnamerecord(query, callback)</td>
    <td style="padding:15px">getDnamerecord</td>
    <td style="padding:15px">{base_path}/{version}/record:dname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDnamerecord(query, body, callback)</td>
    <td style="padding:15px">createDnamerecord</td>
    <td style="padding:15px">{base_path}/{version}/record:dname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnamerecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDnamerecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateDnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnamerecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteDnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnskeyrecord(query, callback)</td>
    <td style="padding:15px">getDnskeyrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:dnskey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnskeyrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDnskeyrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dnskey/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDsrecord(query, callback)</td>
    <td style="padding:15px">getDsrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:ds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDsrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDsrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:ds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDsrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteDsrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:ds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDtclbdnrecord(query, callback)</td>
    <td style="padding:15px">getDtclbdnrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:dtclbdn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDtclbdnrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getDtclbdnrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:dtclbdn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostIpv4Addrrecord(query, callback)</td>
    <td style="padding:15px">getHostIpv4Addrrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:host_ipv4addr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostIpv4AddrrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getHostIpv4AddrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:host_ipv4addr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHostIpv4AddrrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateHostIpv4AddrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:host_ipv4addr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostIpv6Addrrecord(query, callback)</td>
    <td style="padding:15px">getHostIpv6Addrrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:host_ipv6addr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostIpv6AddrrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getHostIpv6AddrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:host_ipv6addr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHostIpv6AddrrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateHostIpv6AddrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:host_ipv6addr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMxrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getMxrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMxrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateMxrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMxrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteMxrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNaptrrecord(query, callback)</td>
    <td style="padding:15px">getNaptrrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:naptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNaptrrecord(query, body, callback)</td>
    <td style="padding:15px">createNaptrrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:naptr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNaptrrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNaptrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNaptrrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNaptrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNaptrrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNaptrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:naptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:ns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNsrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateNsrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:ns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNsrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteNsrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:ns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsecrecord(query, callback)</td>
    <td style="padding:15px">getNsecrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:nsec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsecrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsecrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:nsec/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsec3record(query, callback)</td>
    <td style="padding:15px">getNsec3record</td>
    <td style="padding:15px">{base_path}/{version}/record:nsec3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsec3recordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsec3recordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:nsec3/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsec3paramrecord(query, callback)</td>
    <td style="padding:15px">getNsec3paramrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:nsec3param?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNsec3paramrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getNsec3paramrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:nsec3param/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPtrrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getPtrrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:ptr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRrsigrecord(query, callback)</td>
    <td style="padding:15px">getRrsigrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:rrsig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRrsigrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getRrsigrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:rrsig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrvrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSrvrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSrvrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSrvrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSrvrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSrvrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTlsarecord(query, callback)</td>
    <td style="padding:15px">getTlsarecord</td>
    <td style="padding:15px">{base_path}/{version}/record:tlsa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTlsarecord(query, body, callback)</td>
    <td style="padding:15px">createTlsarecord</td>
    <td style="padding:15px">{base_path}/{version}/record:tlsa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTlsarecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getTlsarecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:tlsa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTlsarecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateTlsarecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:tlsa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTlsarecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteTlsarecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:tlsa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTxtrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getTxtrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:txt/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTxtrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateTxtrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:txt/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTxtrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteTxtrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:txt/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnknownrecord(query, callback)</td>
    <td style="padding:15px">getUnknownrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:unknown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUnknownrecord(query, body, callback)</td>
    <td style="padding:15px">createUnknownrecord</td>
    <td style="padding:15px">{base_path}/{version}/record:unknown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnknownrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getUnknownrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:unknown/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUnknownrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateUnknownrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:unknown/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnknownrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteUnknownrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/record:unknown/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecordNamePolicy(query, callback)</td>
    <td style="padding:15px">getRecordNamePolicy</td>
    <td style="padding:15px">{base_path}/{version}/recordnamepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRecordNamePolicy(query, body, callback)</td>
    <td style="padding:15px">createRecordNamePolicy</td>
    <td style="padding:15px">{base_path}/{version}/recordnamepolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecordNamePolicyByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getRecordNamePolicyByReference</td>
    <td style="padding:15px">{base_path}/{version}/recordnamepolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRecordNamePolicyByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateRecordNamePolicyByReference</td>
    <td style="padding:15px">{base_path}/{version}/recordnamepolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRecordNamePolicyByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteRecordNamePolicyByReference</td>
    <td style="padding:15px">{base_path}/{version}/recordnamepolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleset(query, callback)</td>
    <td style="padding:15px">getRuleset</td>
    <td style="padding:15px">{base_path}/{version}/ruleset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRuleset(query, body, callback)</td>
    <td style="padding:15px">createRuleset</td>
    <td style="padding:15px">{base_path}/{version}/ruleset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRulesetByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getRulesetByReference</td>
    <td style="padding:15px">{base_path}/{version}/ruleset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRulesetByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateRulesetByReference</td>
    <td style="padding:15px">{base_path}/{version}/ruleset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRulesetByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteRulesetByReference</td>
    <td style="padding:15px">{base_path}/{version}/ruleset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScavengingTask(query, callback)</td>
    <td style="padding:15px">getScavengingTask</td>
    <td style="padding:15px">{base_path}/{version}/scavengingtask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScavengingTaskByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getScavengingTaskByReference</td>
    <td style="padding:15px">{base_path}/{version}/scavengingtask/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedArecord(query, callback)</td>
    <td style="padding:15px">getSharedArecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedArecord(query, body, callback)</td>
    <td style="padding:15px">createSharedArecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:a?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedArecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedArecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedArecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:a/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedAAAArecord(query, callback)</td>
    <td style="padding:15px">getSharedAAAArecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:aaaa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedAAAArecord(query, body, callback)</td>
    <td style="padding:15px">createSharedAAAArecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:aaaa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedAAAArecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedAAAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedAAAArecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedAAAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedAAAArecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedAAAArecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:aaaa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedCnamerecord(query, callback)</td>
    <td style="padding:15px">getSharedCnamerecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedCnamerecord(query, body, callback)</td>
    <td style="padding:15px">createSharedCnamerecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:cname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedCnamerecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedCnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedCnamerecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedCnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedCnamerecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedCnamerecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:cname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedMxrecord(query, callback)</td>
    <td style="padding:15px">getSharedMxrecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:mx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedMxrecord(query, body, callback)</td>
    <td style="padding:15px">createSharedMxrecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:mx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedMxrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedMxrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedMxrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedMxrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedMxrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedMxrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:mx/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedSrvrecord(query, callback)</td>
    <td style="padding:15px">getSharedSrvrecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:srv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedSrvrecord(query, body, callback)</td>
    <td style="padding:15px">createSharedSrvrecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:srv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedSrvrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedSrvrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedSrvrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedSrvrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedSrvrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedSrvrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:srv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedTxtrecord(query, callback)</td>
    <td style="padding:15px">getSharedTxtrecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:txt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedTxtrecord(query, body, callback)</td>
    <td style="padding:15px">createSharedTxtrecord</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:txt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedTxtrecordByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedTxtrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:txt/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedTxtrecordByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedTxtrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:txt/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedTxtrecordByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedTxtrecordByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecord:txt/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedRecordGroup(query, callback)</td>
    <td style="padding:15px">getSharedRecordGroup</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecordgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSharedRecordGroup(query, body, callback)</td>
    <td style="padding:15px">createSharedRecordGroup</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecordgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSharedRecordGroupByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getSharedRecordGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecordgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSharedRecordGroupByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateSharedRecordGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecordgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSharedRecordGroupByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteSharedRecordGroupByReference</td>
    <td style="padding:15px">{base_path}/{version}/sharedrecordgroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createView(query, body, callback)</td>
    <td style="padding:15px">createView</td>
    <td style="padding:15px">{base_path}/{version}/view?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getViewByReference</td>
    <td style="padding:15px">{base_path}/{version}/view/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createViewByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">createViewByReference</td>
    <td style="padding:15px">{base_path}/{version}/view/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateViewByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateViewByReference</td>
    <td style="padding:15px">{base_path}/{version}/view/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteViewByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteViewByReference</td>
    <td style="padding:15px">{base_path}/{version}/view/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneAuth(query, callback)</td>
    <td style="padding:15px">getZoneAuth</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneAuth(query, body, callback)</td>
    <td style="padding:15px">createZoneAuth</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateZoneAuthByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateZoneAuthByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneAuthDiscrepancy(query, callback)</td>
    <td style="padding:15px">getZoneAuthDiscrepancy</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth_discrepancy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneAuthDiscrepancyByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getZoneAuthDiscrepancyByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_auth_discrepancy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneDelegatedByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getZoneDelegatedByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_delegated/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneDelegatedByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">createZoneDelegatedByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_delegated/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateZoneDelegatedByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateZoneDelegatedByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_delegated/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZoneDelegatedByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteZoneDelegatedByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_delegated/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneForwardByReference(refInfo, query, callback)</td>
    <td style="padding:15px">getZoneForwardByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_forward/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneForwardByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">createZoneForwardByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_forward/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateZoneForwardByReference(refInfo, query, body, callback)</td>
    <td style="padding:15px">updateZoneForwardByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_forward/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZoneForwardByReference(refInfo, callback)</td>
    <td style="padding:15px">deleteZoneForwardByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_forward/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneRpByReference(zoneRpReference, query, callback)</td>
    <td style="padding:15px">getZoneRpByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_rp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneRpByReference(zoneRpReference, query, body, callback)</td>
    <td style="padding:15px">createZoneRpByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_rp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateZoneRpByReference(zoneRpReference, query, body, callback)</td>
    <td style="padding:15px">updateZoneRpByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_rp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZoneRpByReference(zoneRpReference, callback)</td>
    <td style="padding:15px">deleteZoneRpByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_rp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneStub(query, callback)</td>
    <td style="padding:15px">getZoneStub</td>
    <td style="padding:15px">{base_path}/{version}/zone_stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneStub(query, body, callback)</td>
    <td style="padding:15px">createZoneStub</td>
    <td style="padding:15px">{base_path}/{version}/zone_stub?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZoneStubByReference(zoneStubReference, query, callback)</td>
    <td style="padding:15px">getZoneStubByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_stub/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createZoneStubByReference(zoneStubReference, query, body, callback)</td>
    <td style="padding:15px">createZoneStubByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_stub/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateZoneStubByReference(zoneStubReference, query, body, callback)</td>
    <td style="padding:15px">updateZoneStubByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_stub/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZoneStubByReference(zoneStubReference, callback)</td>
    <td style="padding:15px">deleteZoneStubByReference</td>
    <td style="padding:15px">{base_path}/{version}/zone_stub/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkZoneAssociations(networkId, networkView, query, callback)</td>
    <td style="padding:15px">getNetworkZoneAssociations</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkZoneAssociation(networkId, networkView, query, body, callback)</td>
    <td style="padding:15px">addNetworkZoneAssociation</td>
    <td style="padding:15px">{base_path}/{version}/network/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
