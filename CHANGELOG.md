
## 1.15.5 [11-19-2024]

* change order of params to fix createHostRecord2

See merge request itentialopensource/adapters/adapter-infoblox!49

---

## 1.15.4 [10-15-2024]

* Changes made at 2024.10.14_21:30PM

See merge request itentialopensource/adapters/adapter-infoblox!48

---

## 1.15.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-infoblox!46

---

## 1.15.2 [08-15-2024]

* Changes made at 2024.08.14_19:49PM

See merge request itentialopensource/adapters/adapter-infoblox!45

---

## 1.15.1 [08-07-2024]

* Changes made at 2024.08.06_21:54PM

See merge request itentialopensource/adapters/adapter-infoblox!44

---

## 1.15.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-infoblox!43

---

## 1.14.11 [03-26-2024]

* Changes made at 2024.03.26_14:49PM

See merge request itentialopensource/adapters/inventory/adapter-infoblox!42

---

## 1.14.10 [03-21-2024]

* Changes made at 2024.03.21_14:40PM

See merge request itentialopensource/adapters/inventory/adapter-infoblox!41

---

## 1.14.9 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/inventory/adapter-infoblox!40

---

## 1.14.8 [03-13-2024]

* Changes made at 2024.03.13_14:05PM

See merge request itentialopensource/adapters/inventory/adapter-infoblox!39

---

## 1.14.7 [03-11-2024]

* Changes made at 2024.03.11_14:14PM

See merge request itentialopensource/adapters/inventory/adapter-infoblox!38

---

## 1.14.6 [02-26-2024]

* Changes made at 2024.02.26_13:43PM

See merge request itentialopensource/adapters/inventory/adapter-infoblox!37

---

## 1.14.5 [12-24-2023]

* update metadata

See merge request itentialopensource/adapters/inventory/adapter-infoblox!36

---

## 1.14.4 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/inventory/adapter-infoblox!35

---

## 1.14.3 [12-14-2023]

* Update adapter-utils version

See merge request itentialopensource/adapters/inventory/adapter-infoblox!34

---

## 1.14.2 [11-30-2023]

* Update adapter-utils version

See merge request itentialopensource/adapters/inventory/adapter-infoblox!34

---

## 1.14.1 [11-06-2023]

* Add empty response handling in some tasks

See merge request itentialopensource/adapters/inventory/adapter-infoblox!33

---

## 1.14.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/inventory/adapter-infoblox!32

---

## 1.13.2 [09-27-2023]

* more metadata changes

See merge request itentialopensource/adapters/inventory/adapter-infoblox!31

---

## 1.13.1 [09-27-2023]

* more metadata changes

See merge request itentialopensource/adapters/inventory/adapter-infoblox!31

---

## 1.13.0 [09-20-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-infoblox!29

---

## 1.12.4 [05-18-2023]

* added call to list network containers

See merge request itentialopensource/adapters/inventory/adapter-infoblox!28

---

## 1.12.3 [04-06-2023]

* updated adapter utils version

See merge request itentialopensource/adapters/inventory/adapter-infoblox!26

---

## 1.12.2 [03-24-2023]

* updated adapter utils version

See merge request itentialopensource/adapters/inventory/adapter-infoblox!26

---

## 1.12.1 [11-30-2022]

* Adapt 2543 Add and Update record calls

See merge request itentialopensource/adapters/inventory/adapter-infoblox!25

---

## 1.12.0 [08-24-2022]

* Added 2 calls

See merge request itentialopensource/adapters/inventory/adapter-infoblox!24

---

## 1.11.0 [08-08-2022]

* Add over 200 DNS Calls

See merge request itentialopensource/adapters/inventory/adapter-infoblox!23

---

## 1.10.1 [08-02-2022]

* Added tasks with general options input parameter.

See merge request itentialopensource/adapters/inventory/adapter-infoblox!21

---

## 1.10.0 [05-30-2022]

* Added calls and migrated to latest foundation

See merge request itentialopensource/adapters/inventory/adapter-infoblox!19

---

## 1.9.3 [03-07-2021]

- migration to the latest adapter foundation - also cleanup a lot of manually added methods
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-infoblox!18

---

## 1.9.2 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-infoblox!17

---

## 1.9.1 [05-18-2020]

- assignNextNetworkByNetwork and assignNextNetworkByRef

See merge request itentialopensource/adapters/inventory/adapter-infoblox!16

---

## 1.9.0 [05-13-2020]

- Added new assignNextNetworkv2 call that allows the network view and the return fields to be passed in.

See merge request itentialopensource/adapters/inventory/adapter-infoblox!15

---

## 1.8.0 [04-21-2020] & 1.7.0 [03-23-2020]

- Add new call for getHostKeysByFilter
- Also changed dependencies based on security vulnerabilities
- Still have a low vulnerability (in a dependency of a dependency) so had to allow that for now. 

See merge request itentialopensource/adapters/inventory/adapter-infoblox!14

---

## 1.6.0 [03-05-2020]

- Adds in the restartServices and deleteNetwork calls

See merge request itentialopensource/adapters/inventory/adapter-infoblox!13

---

## 1.5.4 [02-25-2020]

- Add the API location to the README.md

See merge request itentialopensource/adapters/inventory/adapter-infoblox!12

---

## 1.5.3 [01-28-2020]

- Remove the result key from 4 of the calls that had it in there (believe these are from the old way things were built). Also needed to change some of the mock data files to pass valid data.

See merge request itentialopensource/adapters/inventory/adapter-infoblox!10

---

## 1.5.2 [01-28-2020]

- We found that while infoBlox and the adapter wanted a parameter to be a number, it was put in the pronghorn.json as a string and so IAP was sending a string. This could be a breaking change, however, we believe this call never worked and just was not tested integrated before so we are not considering it to be breaking but fixing.

See merge request itentialopensource/adapters/inventory/adapter-infoblox!9

---

## 1.5.1 [01-14-2020]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-infoblox!8

---

## 1.5.0 [01-14-2020]

- Added some useful functions to speed up CRUD operations against Infoblox:
  - Added generic object CRUD entity with methods createObject, updateObject, deleteObject and getObject.
  - Added a workflow to test these CRUD functions.
  - Added a limit to healthCheck entitypath so that the system isn't required to return all the host records available using _max_results=1
  - Also fixed base path for healthcheck action. It was missing /wapi/

See merge request itentialopensource/adapters/inventory/adapter-infoblox!7

---

## 1.4.0 [11-11-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/inventory/adapter-infoblox!6

---

## 1.3.0 [09-19-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-infoblox!4

---
## 1.2.0 [07-31-2019] & 1.1.0 [07-31-2019]

- Migrates the adapter to the latest adapter foundation, categorizes it, makes it available to app artifact and it adds many new calls to the adapter. These new calls where taken from a postman collection converted to swagger while the old calls were merged in from the old adapter avoiding the swagger instances of these calls to avoid breaking changes.

See merge request itentialopensource/adapters/inventory/adapter-infoblox!3

---

## 1.0.6 [05-30-2019]

- Initial Commit

See commit ad204ae

---
