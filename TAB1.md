# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Infoblox System. The API that was used to build the adapter for Infoblox is usually available in the report directory of this adapter. The adapter utilizes the Infoblox API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Infoblox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Infoblox. With this adapter you have the ability to perform operations such as:

- Add and Remove Inventory Items
- Check Availability of Items
- Assign and Unassign Items

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
